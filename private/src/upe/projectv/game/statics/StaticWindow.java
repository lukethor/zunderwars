/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package upe.projectv.game.statics;

import android.util.Log;
import min3d.core.Scene;

/**
 * This class is static so that it's easily accessible. it contains the width
 * and height of the screen along with some convenient functions to do some
 * calculations which tell, for instance, villains how far they can move or
 * the background how much it should rotate, etc.
 */
public class StaticWindow {
    
    // this is the window width and height in pixels
    public static int window_width = 0;
    public static int window_height = 0;
    
    // these are calculated by Min3d to calculate how the screen looks.
    // we will calculate these values so that other places (HeroManager,
    // BackgroundManager, etc.) will have access to them.
    public static float lt = 0,rt = 0,btm = 0,top = 0,aspectRatio = 0;
    
    // this is the FrustumManaged values. these values are hard-coded in the
    // class, so we just put them here for ease of calculation. if you change
    // the values in FrustumManaged, they need to be changed here as well.
    public static float shortSideLength = 1;
    public static float nearPlane = 1;
    public static float farPlane = 100;
    public static float horizontalCenter = 0;
    public static float verticalCenter = 0;
    
    // this is the x position that the hero is restricted to
    public static float herox = 0;
    
    // the scene/camera information for standard view
    public static float camposx = 0,camposy = 10.5f,camposz = 19;
    public static float camupx = 0,camupy = 1,camupz = 0;
    public static float camtargetx = 0,camtargety = 7.5f,camtargetz = -5;
    
    // this is the standard calculation that Min3d does to calculate the 
    // dimensions of the screen. we will calculate and store them here to
    // provide easy access to the other classes that need them
    public static void calculateScreenDimensions() {
        
        // load the frustrum info. this is from Renderer.updateViewFrustrum
        //FrustumManaged vf = scene.camera().frustum;
	//float n = vf.shortSideLength() / 2f;
        float n = shortSideLength / 2f;

	//float lt, rt, btm, top;
	//float aspectRatio = (float)w / (float)h;
        aspectRatio = (float)window_width / (float)window_height;
        
	lt  = horizontalCenter - n * aspectRatio;
        rt  = horizontalCenter + n * aspectRatio;
	btm = verticalCenter - n * 1; 
	top = verticalCenter + n * 1;

	if(aspectRatio > 1) {
            lt *= 1f / aspectRatio;
            rt *= 1f / aspectRatio;
            btm *= 1f / aspectRatio;
            top *= 1f / aspectRatio;
	}
    }
    
    // this function, given y and z usually of a villain, calulates the x value
    // where the screen ends. this way, in the levels we can tell them how far
    // before the screen ends.
    public static float getScreenWidthAtZ(float z) {
        
        // calculate the triangle
        float theta = (float)Math.atan
                (StaticWindow.lt / StaticWindow.nearPlane);
        
        return (Math.abs((float)Math.tan(theta) * 
                Math.abs(z - camposz)));
    }
}
