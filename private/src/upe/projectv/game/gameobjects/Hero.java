package upe.projectv.game.gameobjects;

import min3d.Shared;
import min3d.core.Object3dContainer;
import min3d.vos.Number3d;

import min3d.core.Object3dContainer;
import min3d.parser.IParser;
import min3d.parser.Parser;

public class Hero implements iShip {
    
    private Object3dContainer obj;
    
    private iExplosion explosion;
    
    private Number3d translate;
    private Number3d rotate;
    private Number3d scale;
    
    private float[] bounds;
    private float scalingFactor;
    
    private int status;
    
    private int shotDelay;
    
    public Hero() {
        scalingFactor = 1.0f;
        // min, max x : -2.2042663,2.2042663
        // min, max y : -0.7050826,1.6511254
        // min, max z : -2.764191,2.7332985
        
        // min x, max x, min z, max z
        bounds = new float[] {-2.2042663f * scalingFactor,
            2.2042663f * scalingFactor,
            -2.764191f * scalingFactor,
            2.7332985f * scalingFactor};
        
        // set default values 
        translate = new Number3d(0,0,-7.2f);
        rotate = new Number3d(0,0,0);
        scale = new Number3d(scalingFactor,scalingFactor,scalingFactor);
        
        status = 0;
        
        shotDelay = 0;
    }
    
    @Override
    public void setObject3dContainer(Object3dContainer o) {
        
        obj = o.clone();
        
        obj.position().setAllFrom(translate);
        obj.rotation().setAllFrom(rotate);
        obj.scale().setAllFrom(scale);
    }
    
    @Override
    public iExplosion getExplosion() {
        return explosion;
    }
    
    @Override
    public void setExplosion(iExplosion explosion) {
        this.explosion = explosion;
    }
    
    // before going into pause, we need to save the current status of the
    // 3D object (position, rotation, etc.)
    @Override
    public void saveStatus() {
        translate.setAllFrom(obj.position());
        rotate.setAllFrom(obj.rotation());
    }
    
    @Override
    public Object3dContainer getObject3dContainer() {
        return obj;
    }
    
    @Override
    public void setShipTranslation(Number3d n) {
        translate.setAllFrom(n);
    }
    
    @Override
    public void setObjTranslation(Number3d n) {
        obj.position().setAllFrom(n);
    }
    
    @Override
    public float[] getBoundingBox() {
        return bounds;
    }
    
    @Override
    public Number3d getLocation() {
        //return translate;
        return obj.position();
    }
    
    @Override
    public void setStatus(int status) {
        this.status = status;
    }
    
    @Override
    public int getStatus() {
        
        return status;
    }
    
    @Override
    public void setGroupNumber(int group) {
        
    }
    
    @Override
    public int getGroupNumber() {
        return 0;
    }
    
    @Override
    public int getShotDelayCount() {
        
        return shotDelay;
    }
    
    @Override
    public void setShotDelayCount(int shotDelay) {
        
        this.shotDelay = shotDelay;
    }
    
    @Override
    public int getType() {
        return 0;
    }
}
