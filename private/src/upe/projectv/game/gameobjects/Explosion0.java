package upe.projectv.game.gameobjects;

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import min3d.vos.TextureVo;
import min3d.vos.TexEnvxVo;
import javax.microedition.khronos.opengles.GL10;

public class Explosion0 implements iExplosion {
    
    private Object3dContainer obj;
    
    private TextureVo texVo;
    private TexEnvxVo texEnv;
    
    private Number3d translate;
    private Number3d rotate;
    private Number3d scale;
    
    private float[] bounds;
    private float scalingFactor;
    
    int frameNo = 0;
    int subFrameNo = 0;
    int frameRate = 1;
    
    public Explosion0() {
        
        scalingFactor = 1.0f;
        
        // set default values 
        translate = new Number3d(0,1.0f,-6.0f);
        //rotate = new Number3d(180,0,0);
        rotate = new Number3d(180,0,0);
        scale = new Number3d(scalingFactor,scalingFactor,scalingFactor);
    }
    
    @Override
    public int cycle() {
        
        if(subFrameNo < frameRate)
            subFrameNo++;
        else {
            String frame = "ex1_" + frameNo;
            texVo = new TextureVo(frame);
            obj.textures().addReplace(texVo);
            texEnv = texVo.textureEnvs.get(0);
            texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE,GL10.GL_REPLACE);
        
            frameNo++;
            subFrameNo = 0;
            
            // if we have hit the peak of the explosion
            // we need to get rid of the ship
            if(frameNo == 8)
                return 1;
        
            if(frameNo == 16) {
                frameNo = 0;
                subFrameNo = 0;
                return 2;
            }
        }
        
        return 0;
    }
    
    // this loads the 3D object. each time we return we'll need to do this
    @Override
    public void setObject3dContainer(Object3dContainer o) {
        
        //obj = o.clone();
        obj = o;
        
        // put in current picture
        texVo = new TextureVo("ex1_" + frameNo);
        obj.textures().addReplace(texVo);
        texEnv = texVo.textureEnvs.get(0);
        texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE,GL10.GL_REPLACE);
        
        // set the position of our object from the values
        // in our saved arrays here
        obj.position().setAllFrom(translate);
        obj.rotation().setAllFrom(rotate);
        obj.scale().setAllFrom(scale);
    }
    
    // before going into pause, we need to save the current status of the
    // 3D object (position, rotation, etc.)
    @Override
    public void saveStatus() {
        translate.setAllFrom(obj.position());
        rotate.setAllFrom(obj.rotation());
    }
    
    @Override
    public Object3dContainer getObject3dContainer() {
        return (Object3dContainer)obj;
    }
    
    @Override
    public void setTranslation(float x,float y,float z) {
        translate.setAll(x,y,z);
    }
}
