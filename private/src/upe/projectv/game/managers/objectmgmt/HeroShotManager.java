package upe.projectv.game.managers.objectmgmt;

import java.util.LinkedList;
import java.util.Iterator;
import upe.projectv.game.gameobjects.iShot;
import upe.projectv.game.gameobjects.Shot0;
import min3d.vos.Number3d;
import min3d.core.Scene;
import java.util.concurrent.Semaphore;
import min3d.core.Object3dContainer;


import min3d.core.Object3dContainer;
import min3d.vos.Number3d;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import min3d.core.Object3d;

public class HeroShotManager {
    
    private Object3dContainer template0;
    
    private LinkedList<iShot> activeShots;
    private LinkedList<iShot> inactiveShots0;
    private Iterator<iShot> shotIterator;
    
    private iShot tempShot;
    private Number3d tempNum3d;
    
    private Scene scene;
    
    private int shotCycleThreshold = 50;
    private int shotCycleCount = 0;
    
    // this is the z location past which we no longer draw the shot so we
    // need to remove the shot from the scene and move it back to inactiveShots
    private int zThreshold = -70;
    
    private float shotSpeed = 0.1f;
    
    // PUT THE SHOT TYPE HERE
    
    public HeroShotManager(Scene scene) {
        
        // set the scene
        this.scene = scene;
        
        // create the linked lists for the shots
        activeShots = new LinkedList<iShot>();
        inactiveShots0 = new LinkedList<iShot>();
        
        // load up some shots in the inactiveShots
        // create Shot_1 instances and add them to inactiveShots_1;
        for(int i = 0; i < 10; i++) {
            tempShot = new Shot0();
            inactiveShots0.addLast(tempShot);
        }
    }
    
    public void emptyActiveQueue() {
        
        shotIterator = activeShots.iterator();
        
        while(shotIterator.hasNext()) {
            
            tempShot = shotIterator.next();
            
            shotIterator.remove();
            
            scene.removeChild(tempShot.getObject3dContainer());
            
            inactiveShots0.add(tempShot);
        }
    }
    
    // this function is called by the GameManager to add shots to the scene.
    // the semaphore is handled above in GameManager.
    public void addShot(Number3d n) {
        
        if (inactiveShots0.isEmpty() || shotCycleCount > 0) {} 
        else {
            shotCycleCount = 1;
            tempShot = inactiveShots0.removeFirst();
            tempShot.setTranslation(n.x,
                    n.y + 0.2f,n.z - 0.7f);
            activeShots.addLast(tempShot);
            // add the shot first so the transparency for the
            // explosions works as anticipated.
            scene.addChildAt(tempShot.getObject3dContainer(), 0);
        }
    }
    
    // this is used to take a shot from the scene and put it back into
    // its inactive queue
    public void removeShot(iShot shot) {
        
        // remove the shot from the scene
        scene.removeChild(shot.getObject3dContainer());
        
        // add the shot back to the inactive container
        inactiveShots0.add(shot);
    }
    
    public void cycle() {
        
        // iterate through the shots and move them
        shotIterator = activeShots.iterator();

        while (shotIterator.hasNext()) {
            tempShot = shotIterator.next();

            // see if the shot has gone too far. if so, remove it from the
            // scene and move it back to inactiveShots
            if (tempShot.getObject3dContainer().position().z < zThreshold) {
                scene.removeChild(tempShot.getObject3dContainer());
                inactiveShots0.addLast(tempShot);
                shotIterator.remove();
            } 
            else {
                tempNum3d = tempShot.getObject3dContainer().position();
                tempShot.setTranslation(tempNum3d.x, tempNum3d.y, 
                        tempNum3d.z - shotSpeed);
            }
        }
    }
    
    // each cycle (gl time to draw) we need to update the shot cycle count
    // this way, we know if the appropriate number of cycles have occurred
    // between each shot
    public void updateShotCycleCount() {
        
        if(shotCycleCount == shotCycleThreshold)
            shotCycleCount = 0;
        else if(shotCycleCount > 0)
            shotCycleCount += 1;
    }
    
    // this function goes through the active shots and saves their current
    // status
    public void saveStatus() {
        
        // iterate through the shots and save them
        shotIterator = activeShots.iterator();

        while (shotIterator.hasNext()) {
            tempShot = shotIterator.next();
            tempShot.saveStatus();
        }
    }
    
    // this function is used to load up the object3dcontainer for active shots
    public void setObject3dContainers() {
        
        // load the template
        template0 = new Rectangle(0.35f,2.0f,1,1,new Color4(0,255,0,255));
        
        // iterate through the shots and load them
        // and add them to the scene
        shotIterator = activeShots.iterator();

        while (shotIterator.hasNext()) {
            tempShot = shotIterator.next();
            tempShot.setObject3dContainer(template0);
            scene.addChild(tempShot.getObject3dContainer());
        }
        
        // we must also load the inactive shots with their
        // textures so they are visible when they are used
        shotIterator = inactiveShots0.iterator();

        while (shotIterator.hasNext()) {
            tempShot = shotIterator.next();
            tempShot.setObject3dContainer(template0);
        }
    }
    
    public void setShotSpeed(float speed) {
        
        this.shotSpeed = speed;
    }
    
    // used by CollisionManager to get the inactiveShots_1
    public LinkedList<iShot> getInactiveShots_1() {
        return inactiveShots0;
    }
    
    // used by CollisionManager to get the activeShots
    public LinkedList<iShot> getActiveShots() {
        return activeShots;
    }
}
