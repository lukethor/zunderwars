package upe.projectv.game.managers.objectmgmt;

import java.util.concurrent.Semaphore;
import upe.projectv.game.gameobjects.Hero;
import android.os.Message;
import min3d.vos.Number3d;
import java.util.LinkedList;
import java.util.Iterator;
import upe.projectv.game.gameobjects.iShot;
import upe.projectv.game.gameobjects.iShip;
import upe.projectv.game.managers.gamemgmt.GameManager;

import android.content.Intent;
import android.util.Log;
import upe.projectv.game.statics.Score;

public class CollisionManager {
    
    private GameManager gm;
    private VillainManager vMgr;
    private VillainShotManager vsMgr;
    private HeroManager hMgr;
    private HeroShotManager hsMgr;
    
    private LinkedList<iShip> activeShips;
    private LinkedList<iShot> activeShots;
    
    private Iterator<iShot> shotIterator;
    private Iterator<iShip> villainIterator;
    
    private iShip tempShip;
    private iShot tempShot;
    
    private float[] tempShipBounds;
    private Number3d tempShipTrans;
    private float[] tempShotBounds;
    private Number3d tempShotTrans;
    
    
    public CollisionManager(GameManager gm) {
        
        this.vMgr = gm.vMgr;
        this.vsMgr = gm.vsMgr;
        this.hMgr = gm.hMgr;
        this.hsMgr = gm.hsMgr;
        this.gm = gm;
    }
    
    /*
    public CollisionManager(VillainManager vMgr,VillainShotManager vsMgr,
            HeroManager hMgr,HeroShotManager hsMgr,ExplosionManager eMgr) {
        
        this.vMgr = vMgr;
        this.vsMgr = vsMgr;
        this.hMgr = hMgr;
        this.hsMgr = hsMgr;
    }
    */
    
    // go through the villains and compare to the shots from the hero
    // to see if there is a collision
    public void detectVillainCollisions() {
        
        // get good guy active and inactive shot LinkedLists
        activeShots = hsMgr.getActiveShots();
        
        // get active and inactive villains
        activeShips = vMgr.getActiveVillains();
        
        // iterate through the bad guys
        villainIterator = activeShips.iterator();
        while (villainIterator.hasNext()) {

            tempShip = villainIterator.next();

            // iterate through shots and compare with each bad guy
            shotIterator = activeShots.iterator();
            while (shotIterator.hasNext()) {

                tempShot = shotIterator.next();

                tempShipBounds = tempShip.getBoundingBox();
                tempShipTrans = tempShip.getLocation();
                tempShotBounds = tempShot.getBoundingBox();
                tempShotTrans = tempShot.getLocation();

                // check the z boundaries
                if (tempShotTrans.z + tempShotBounds[2]
                        <= tempShipTrans.z + tempShipBounds[3]
                        && tempShotTrans.z + tempShotBounds[3]
                        >= tempShipTrans.z + tempShipBounds[2]) {

                    // check the x boundaries
                    if (tempShotTrans.x >= tempShipTrans.x
                            + tempShipBounds[0]
                            && tempShotTrans.x <= tempShipTrans.x
                            + tempShipBounds[1]) {

                        // take the bad guy out of the active queue. it 
                        // will be referenced by the explosion and then
                        // put into the inactive queue when the explosion
                        // has finished
                        villainIterator.remove();
                        vMgr.setVillainToDying(tempShip);

                        // remove shot from the scene and into inactive
                        shotIterator.remove();
                        hsMgr.removeShot(tempShot);
                        
                        // check which type of villain and adjust the points
                        // accordingly. 0 is the saucer, and 1 is spiky
                        if(tempShip.getType() == 0) {
                            
                            Message msg = Message.obtain();
                            msg.arg1 = 103;
                            msg.obj = String.valueOf(Score.increaseScore(100));
                            gm.overlayHandler.sendMessage(msg);
                        }
                        // spiky bad guy
                        else if(tempShip.getType() == 1) {
                            
                            Message msg = Message.obtain();
                            msg.arg1 = 103;
                            msg.obj = String.valueOf(Score.increaseScore(150));
                            gm.overlayHandler.sendMessage(msg);
                        }
                        
                        // fire off the explosion sound
                        //SoundHandler.playExplosionSound(); ********************************************************************
                        Message explosionMsg = Message.obtain();
                        explosionMsg.arg1 = 200;
                        gm.overlayHandler.sendMessage(explosionMsg);
                        
                        // now we need to break out of the while loop. we use
                        // the bounds, so if two shots are colliding with the
                        // same ship it will use the bounds from the ship twice
                        // and try to remove twice, which causes an exception
                        break;
                    }
                }
                // we have determined the z-range is not matching for the shot
                // and the villain. if the shot's z value is greater than that
                // of the villain, we can ignore the remaining shots because
                // they are further away.
                else if(tempShotTrans.z > tempShipTrans.z)
                    break;
            }
        }
        
        //// Log.v("JOEL","Finished detect villain collisions");
    }
    
    // check here for the hero being shot
    public void detectHeroCollisions() {
        
        if(hMgr.getHero().getStatus() != 0) {}
        else {
            
            tempShip = hMgr.getHero();

            // iterate through and see if any of the shots hit the hero
            shotIterator = vsMgr.getActiveShots().iterator();

            while (shotIterator.hasNext()) {

                tempShot = shotIterator.next();

                tempShipBounds = tempShip.getBoundingBox();
                tempShipTrans = tempShip.getLocation();
                tempShotBounds = tempShot.getBoundingBox();
                tempShotTrans = tempShot.getLocation();

                // check the z boundaries
                if (tempShotTrans.z + tempShotBounds[2]
                        <= tempShipTrans.z + tempShipBounds[3]
                        && tempShotTrans.z + tempShotBounds[3]
                        >= tempShipTrans.z + tempShipBounds[2]) {

                    // check the x boundaries
                    if (tempShotTrans.x >= tempShipTrans.x
                            + tempShipBounds[0]
                            && tempShotTrans.x <= tempShipTrans.x
                            + tempShipBounds[1]) {

                        // remove the villains shot from the active queue and 
                        // also from the scene
                        shotIterator.remove();
                        vsMgr.removeShot(tempShot);

                        // set the hero to dying
                        hMgr.setHeroToDying();
                        
                        // fire off the explosion sound
                        //SoundHandler.playExplosionSound(); ********************************************************************
                        Message explosionMsg = Message.obtain();
                        explosionMsg.arg1 = 200;
                        gm.overlayHandler.sendMessage(explosionMsg);
                        
                        // break out of the loop, since we're dead anyway
                        break;
                    }
                }
            }
        }
    }
}
