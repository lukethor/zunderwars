package upe.projectv.game.managers.gamemgmt;

import upe.projectv.game.statics.GLThreadSync;
import upe.projectv.game.levels.iLevel;
import upe.projectv.game.levels.Level0;
import upe.projectv.game.levels.Level1;

import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import android.opengl.GLSurfaceView;
import min3d.vos.CameraVo;
import java.util.concurrent.Semaphore;
import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.managers.objectmgmt.CollisionManager;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import upe.projectv.game.managers.objectmgmt.HeroManager;
import upe.projectv.game.managers.objectmgmt.HeroShotManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.managers.objectmgmt.VillainShotManager;
import min3d.vos.Number3d;
import min3d.core.Scene;
import android.util.Log;
import upe.projectv.game.statics.StaticWindow;
import android.os.Handler;
import android.os.Message;
import upe.projectv.game.statics.Score;

public class GameCycle {
    
    private iLevel level;
    private int levelCounter;
    private final int numberOfLevels = 3;
    
    private GameManager gm;
    private Handler overlayHandler;
    
    private boolean loadLevelBool;      // used for beginning load levels
    private int loadLevelCounter;
    private boolean loadSceneBool;      // used if paused/resumed, or startup
    private int loadSceneCounter;
    private boolean introWaitBool;      // when waiting before level starts
    private int introWaitCounter;
    private boolean outroWaitBool;      // when waiting after level ends
    private int outroWaitCounter;
    private boolean gameOverBool;
    private int gameOverCounter;
    private boolean playing;
    private boolean paused;
    private int pausedCounter;
    
    private Thread loadLevelThread;
    private LoadLevel tLoadLevel;
    private LoadCharacters tLoadObj3d;
    
    
    private Rectangle testrect;
    
    // number of cycles in between collision detections
    // 0 means check every cycle, 1 every other, etc.
    // DO NOT PUT LESS THAN 0!!!!
    private int collisionCycles = 4;
    private int collisionCyclesCounter;
    
    // for the multi-threaded logic unit
    private Thread t;
    private LogicThread lThread;
    
    private final int fps = 30;
    private final int maxSkippedFrames = 5;
    private final int framePeriod = 1000 / fps;
    private int numSkippedFrames;
    private int sleepTime;
    private long startTime,timeDifference;
    
    
    public GameCycle(GameManager gm,Handler overlayHandler) {
        
        this.gm = gm;
        this.overlayHandler = overlayHandler;
        
        levelCounter = 0;
        loadLevelBool = true;
        loadLevelCounter = 0;
        loadSceneBool = true;
        loadSceneCounter = 0;
        introWaitBool = false;
        introWaitCounter = 0;
        outroWaitBool = false;
        outroWaitCounter = 0;
        gameOverBool = false;
        gameOverCounter = 0;
        playing = false;
        paused = false;
        pausedCounter = 0;
    }
    
    private class LogicThread implements Runnable {
        
        private boolean running;
        
        public void stopLogicThread() {
            running = false;
        }
        
        @Override
        public void run() {
            
            running = true;
            
            while(running) {
                
                // reset skipped frames
                numSkippedFrames = 0;
                
                // get the current time
                startTime = System.currentTimeMillis();
                
                // do the update and the draw
                if(!GLThreadSync.getGLSemaphore()) {}
                else {
                    
                    updateScene();
                    GLThreadSync.releaseGLSemaphore();
                    
                    gm.glSurfaceView.requestRender();
                 
                    // check how we did for time
                    timeDifference = System.currentTimeMillis() - startTime;

                    // subtract this from the number of milliseconds in a period 
                    // to see how long to sleep (or how far over we are)
                    sleepTime = framePeriod - (int) timeDifference;
                    
                    //// Log.v("JOEL","sleep time : " + sleepTime);

                    // we're ahead of schedule, go to sleep
                    if (sleepTime > 0) {

                        try {Thread.sleep(sleepTime);}
                        catch (Exception e) {}
                    }
                    
                    // if we're behind schedule, we need to skip some frames
                    else if (sleepTime < 0) {

                        while (numSkippedFrames < maxSkippedFrames
                                && sleepTime < 0) {
                            
                            // do the update and the draw
                            if(!GLThreadSync.getGLSemaphore()) {}
                            else {
                                // do the update
                                updateScene();
                                
                                GLThreadSync.releaseGLSemaphore();
                            }
                            
                            // increment skipped frames counter
                            numSkippedFrames += 1;

                            // increment the overage time difference by the 
                            // ideal frame period. this tells us how many 
                            // frames to skip.
                            sleepTime += framePeriod;
                        }
                    }
                }
            }
        }
    }
    
    // copies from the activity. since everything happens in GameManager we'll
    // just call the function from here instead of working it in the Activity
    public void updateScene() {
        
        // get the onPause semaphore
        if (gm.getOnPauseSemaphore()) {
            
            // if the game is not playing, go here. since most cycles are going 
            // to be playing cycles, get to playing as quickly as possible
            if (!playing) {

                // if we have to redo the loading of characters, it goes here. 
                // this sets up the scene so we can do the loading screen.
                if (loadSceneBool) {

                    if (loadSceneCounter == 0) {

                        // Log.v("JOEL", "Loading Characters");

                        // set the scene up
                        gm.scene.lightingEnabled(false);

                        // set camera position - std game view
                        gm.scene.camera().position = new Number3d
                                (StaticWindow.camposx,
                                StaticWindow.camposy,
                                StaticWindow.camposz);
                        gm.scene.camera().upAxis = new Number3d
                                (StaticWindow.camupx,
                                StaticWindow.camupy,
                                StaticWindow.camupz);
                        gm.scene.camera().target = new Number3d
                                (StaticWindow.camtargetx,
                                StaticWindow.camtargety,
                                StaticWindow.camtargetz);
                        
                        // clear the screen
                        Message msg = Message.obtain();
                        msg.arg1 = 104;
                        overlayHandler.sendMessage(msg);
                        
                        // set up the overlay text to say loading
                        Message msg2 = Message.obtain();
                        msg2.arg1 = 100;
                        msg2.obj = "Loading";
                        overlayHandler.sendMessage(msg2);

                        // set the score text value
                        Message msg3 = Message.obtain();
                        msg3.arg1 = 103;
                        msg3.obj = String.valueOf(Score.getScore());
                        overlayHandler.sendMessage(msg3);
                        
                        /*
                        // remove previous rectangle
                        if (testrect != null) {
                            gm.scene.removeChild(testrect);
                            testrect = null;
                        }

                        // add the loading graphic
                        Color4 color = new Color4(255, 0, 0, 255);
                        testrect = new Rectangle(2, 2, 1, 1, color);
                        testrect.position().setAll(0, 0, -15);
                        testrect.rotation().setAll(180, 0, 0);
                        testrect.scale().setAll(1, 1, 1);
                        gm.scene.addChild(testrect);
                        */
                        
                        loadSceneCounter = 1;
                    } 
                    else {

                        // if the thread has finished loading, change so we can 
                        // load the level
                        if (tLoadObj3d.isDone()) {

                            // Log.v("JOEL", "Loading Characters Done");
                            loadSceneBool = false;
                            loadSceneCounter = 0;
                            collisionCyclesCounter = 0;
                        }
                        /*
                        // otherwise, sleep so that the GL thread has some time
                        // to work
                        else {
                            try{Thread.sleep(200);} catch(Exception e) {}
                        }
                        */
                    }
                } 
                // if we're loading the level
                else if (loadLevelBool) {

                    // if we're setting up the thread, first time through
                    if (loadLevelCounter == 0) {

                        // Log.v("JOEL", "Loading Level");

                        
                        // clear the screen
                        Message msg = Message.obtain();
                        msg.arg1 = 104;
                        overlayHandler.sendMessage(msg);
                        
                        // set up the overlay text to say loading
                        Message msg2 = Message.obtain();
                        msg2.arg1 = 100;
                        msg2.obj = "Loading";
                        overlayHandler.sendMessage(msg2);
                        
                        
                        /*
                        // remove previous rectangle
                        if (testrect != null) {
                            gm.scene.removeChild(testrect);
                            testrect = null;
                        }

                        // add the loading graphic
                        Color4 color = new Color4(0, 255, 0, 255);
                        testrect = new Rectangle(2, 2, 1, 1, color);
                        testrect.position().setAll(0, 0, -15);
                        testrect.rotation().setAll(180, 0, 0);
                        testrect.scale().setAll(1, 1, 1);
                        gm.scene.addChild(testrect);
                        */
                        
                        if (levelCounter == 0) {
                            level = new Level0(gm);
                        } 
                        else if (levelCounter == 1) {
                            level = new Level1(gm);
                        }

                        tLoadLevel = new LoadLevel(gm, level);
                        gm.glSurfaceView.queueEvent(tLoadLevel);

                        loadLevelCounter = 1;
                    } 
                    // if the thread is loading, we're waiting for it to finish
                    else {

                        // if the thread has finished loading, change so we can 
                        // load the characters
                        if (tLoadLevel.isDone()) {

                            // Log.v("JOEL", "Loading Level Done");
                            loadLevelBool = false;
                            loadLevelCounter = 0;
                            
                            // set the level value
                            Message msg = Message.obtain();
                            msg.arg1 = 102;
                            msg.obj = String.valueOf(levelCounter + 1);
                            overlayHandler.sendMessage(msg);

                            introWaitBool = true;

                            try {
                                loadLevelThread.join();
                            } 
                            catch (Exception e) {}
                        }
                        /*
                        // otherwise, sleep so that the GL thread has some time
                        // to work
                        else {
                            try{Thread.sleep(200);} catch(Exception e) {}
                        }
                        */
                    }
                } 
                // if we're paused, don't do either playing or the waitTimes
                else if (paused) {

                    // if this is the first time into pause
                    if (pausedCounter == 0) {
                        
                        // display paused
                        Message msg = Message.obtain();
                        msg.arg1 = 100;
                        msg.obj = "Paused";
                        overlayHandler.sendMessage(msg);
                        
                        // show the overlay
                        Message msg2 = Message.obtain();
                        msg2.arg1 = 105;
                        overlayHandler.sendMessage(msg2);
                        
                        // clear the touch queue
                        gm.getShotsFromQueue();

                        pausedCounter = 1;
                    } 
                    else {

                        // check the motion for a hit
                        try {

                            if (gm.getShotsFromQueue() > 0) {

                                playing = true;
                                paused = false;
                                pausedCounter = 0;

                                // we're coming out of a pause 
                                // so start the audio
                                //SoundHandler.playBeepSound(); ********************************************************************
                                
                                
                                Message msg = Message.obtain();
                                msg.arg1 = 100;
                                msg.obj = "";
                                overlayHandler.sendMessage(msg);
                                
                                /*
                                // remove previous graphic
                                if (testrect != null) {
                                    gm.scene.removeChild(testrect);
                                    testrect = null;
                                }
                                */
                            }
                        } 
                        catch (Exception e) {}
                    }
                } 
                // if we're doing the intro wait times
                else if (introWaitBool) {

                    // if this is the first time here, we need to do set up
                    if (introWaitCounter == 0) {
                        
                        // show the overlay
                        Message msg = Message.obtain();
                        msg.arg1 = 105;
                        overlayHandler.sendMessage(msg);
                        
                        Message msg2 = Message.obtain();
                        msg2.arg1 = 100;
                        msg2.obj = "Get Ready";
                        overlayHandler.sendMessage(msg2);
                        
                        // really just for the first time the app starts. this
                        // unhides the container
                        Message msg3 = Message.obtain();
                        msg3.arg1 = 107;
                        overlayHandler.sendMessage(msg3);
                        
                        // we need to start the background beeping noise
                        //SoundHandler.playBeepSound(); ********************************************************************

                        introWaitCounter = 1;
                    } 
                    // if we're done with the wait time
                    else if (introWaitCounter > 100) {

                        
                        Message msg = Message.obtain();
                        msg.arg1 = 100;
                        msg.obj = "";
                        overlayHandler.sendMessage(msg);
                        
                        
                        /*
                        // remove existing test rect
                        if (testrect != null) {
                            gm.scene.removeChild(testrect);
                            testrect = null;
                        }
                        */
                        
                        introWaitBool = false;
                        introWaitCounter = 0;
                        playing = true;
                    } 
                    // otherwise just increment the counter
                    else {
                        introWaitCounter += 1;
                    }
                    
                    // have the villains move during this process
                    level.introVillainAI(gm.vMgr);
                } 
                // if we're doing the outro wait time
                else if (outroWaitBool) {

                    // if this is the first time here, we need to do set up
                    if (outroWaitCounter == 0) {

                        /*
                        // remove existing test rect
                        if (testrect != null) {
                            gm.scene.removeChild(testrect);
                            testrect = null;
                        }
                        */
                        
                        Message msg = Message.obtain();
                        msg.arg1 = 100;
                        msg.obj = "Level Completed";
                        overlayHandler.sendMessage(msg);
                        
                        /*
                        // add waiting test rect
                        Color4 color = new Color4(0, 0, 255, 255);
                        testrect = new Rectangle(2, 2, 1, 1, color);
                        testrect.position().setAll(0, 0, -15);
                        testrect.rotation().setAll(180, 0, 0);
                        testrect.scale().setAll(1, 1, 1);
                        gm.scene.addChild(testrect);
                        */
                        
                        // kill the beeping noise
                        //SoundHandler.stopBeepAudio(); ********************************************************************

                        outroWaitCounter = 1;
                    } 
                    // if we're done with the wait time
                    else if (outroWaitCounter > 100) {

                        
                        Message msg = Message.obtain();
                        msg.arg1 = 100;
                        msg.obj = "";
                        overlayHandler.sendMessage(msg);
                        
                        
                        /*
                        // remove existing test rect
                        if (testrect != null) {
                            gm.scene.removeChild(testrect);
                            testrect = null;
                        }
                        */
                        
                        outroWaitBool = false;
                        outroWaitCounter = 0;

                        // clear everything out of the scene
                        gm.hMgr.clearActiveQueues();
                        gm.hsMgr.emptyActiveQueue();
                        gm.eMgr.clearActiveQueues();
                        gm.vMgr.emptyNonActiveQueues();
                        gm.vsMgr.emptyActiveQueue();
                        gm.backMgr.emptyActiveQueue();

                        // now set it so the next level gets loaded
                        levelCounter = (levelCounter + 1) % numberOfLevels;
                        loadLevelBool = true;
                    } // otherwise just increment the counter
                    else {
                        
                        // move hero back to the center
                        gm.hMgr.recenter();
                        // move any left over hero's shots
                        gm.hsMgr.cycle();
                        
                        outroWaitCounter += 1;
                    }
                }
            } 
            // if we're loading the level
            else if (gameOverBool) {

                // if we're setting up the thread, first time through
                if (gameOverCounter == 0) {                    

                    Message msg = Message.obtain();
                    msg.arg1 = 100;
                    msg.obj = "Game Over";
                    overlayHandler.sendMessage(msg);

                    gameOverCounter = 1;
                } 
                // if we're waiting for the game over part to finish
                else {

                    // if the thread has finished loading, change so we can 
                    // load the characters
                    if (gameOverCounter > 120) {

                        gameOverBool = false;
                        gameOverCounter = 0;
                        
                        // reset values so that we start over
                        levelCounter = 0;
                        loadLevelBool = true;
                        loadLevelCounter = 0;
                        loadSceneBool = false;
                        loadSceneCounter = 0;
                        introWaitBool = false;
                        introWaitCounter = 0;
                        outroWaitBool = false;
                        outroWaitCounter = 0;
                        gameOverBool = false;
                        gameOverCounter = 0;
                        paused = false;
                        pausedCounter = 0;
                        playing = false;                        
                        levelCounter = 0;
                        
                        // clear everything out of the scene
                        gm.hMgr.clearActiveQueues();
                        gm.hsMgr.emptyActiveQueue();
                        gm.eMgr.clearActiveQueues();
                        gm.vMgr.emptyNonActiveQueues();
                        gm.vsMgr.emptyActiveQueue();
                        gm.backMgr.emptyActiveQueue();
                        gm.hMgr.getHero().setStatus(0);
                        
                        // clear the screen
                        Message msg = Message.obtain();
                        msg.arg1 = 104;
                        overlayHandler.sendMessage(msg);
                        
                        
                        gm.hMgr.setLives(3);
                        
                        Message msg2 = Message.obtain();
                        msg2.arg1 = 101;
                        msg2.obj = "3";
                        overlayHandler.sendMessage(msg2);
                        
                        Score.setScore(0);
                    }
                    else
                        gameOverCounter += 1;
                }
            }
            // MAIN GAME PLAYING LOOP
            else {

                // manage the collision detection
                if (collisionCyclesCounter == collisionCycles) {

                    // check for villains being shot
                    gm.cMgr.detectVillainCollisions();

                    // check for the hero being shot
                    gm.cMgr.detectHeroCollisions();

                    collisionCyclesCounter = 0;
                } 
                else
                    collisionCyclesCounter += 1;

                
                
//**************************** HERE IS WHERE TO CHECK FOR HERO BEING ALIVE OR NOT ************************************
//**************************** WILL ALSO NEED TO ADD ANOTHER VALUE INTO HEROMANAGER (OR HERO) FOR CHECKING ***********                
                // here we make sure we're still alive before getting the
                // shot and movement data from the queues. 0 is regular and
                // 3 is during the respawn
                if(gm.hMgr.getHero().getStatus() != 0
                        && gm.hMgr.getHero().getStatus() != 3) {
                
                    // if we're dead and it's game over
                    if(gm.hMgr.getHero().getStatus() == 4) {
                        
                        gameOverBool = true;
                    }
                }
                else {
                    
                    // get the hero's movement from the queue
                    gm.hMgr.androidShifted(gm.getHeroMovement());

                    if (gm.getShotsFromQueue() < 1) {}
                    else {

                        // add shot
                        gm.hMgr.addShot();
                        
                        Message shotMsg = Message.obtain();
                        shotMsg.arg1 = 201;
                        shotMsg.obj = String.valueOf(Score.getScore());
                        overlayHandler.sendMessage(shotMsg);
                        
                        // play sound
                        //SoundHandler.playAudio(1);
                        
                        /*
                        // FOR TESTING PURPOSES/VILLAIN SHOOTS WHEN YOU DO
                        if (vsMgr.getInactiveShots0().peek() != null
                        && vMgr.getActiveVillains().peek() != null) {
                        
                        vsMgr.addShot(vMgr.getActiveVillains().peek()
                        .getObject3dContainer().position());
                        }
                         */
                    }
                }
                

                gm.hsMgr.updateShotCycleCount();
                gm.hsMgr.cycle();

                gm.hMgr.cycle();

                // move the bad guys
                level.gameVillainAI(gm.vMgr,gm.vsMgr);

                // manage dying villains
                gm.vMgr.cycle();

                // manage villain's shots
                gm.vsMgr.cycle();

                // check if the level is finished. this happens when the active
                // villains is empty, the active explosions is empty, and
                // the active shots is also empty
                if (gm.vMgr.getActiveVillains().isEmpty()
                        && gm.eMgr.getActiveExplosions().isEmpty()
                        && gm.vsMgr.getActiveShots().isEmpty()) {

                    outroWaitBool = true;
                    playing = false;
                }
            }

            // release the semaphore
            gm.releaseOnPauseSemaphore();
        }
    }
    
    public void onPause() {
        
        gm.getOnPauseSemaphore();
        
        loadLevelCounter = 0;
        
        loadSceneBool = true;
        loadSceneCounter = 0;
        
        playing = false;
        paused = true;
        pausedCounter = 0;
        
        gm.releaseOnPauseSemaphore();
        
        // kill the load characters thread if it's running
        if(tLoadObj3d != null)
            tLoadObj3d.killThread();
        
        // kill the logic thread that is running then join it
        if(lThread != null)
            lThread.stopLogicThread();
        
        try{t.join();} catch(Exception e) {}
        
        // bug from Eric's phone and my new phone
        // I think when game opens, it has to shift to portrait mode. when this
        // happens if hero and shots are not initialized when it tries to save
        // it gets a null pointer exception. so do a try catch and if it 
        // catches an error, set needInit to true so it will reload from scratch
        // when it gets to portrait mode.
        try {
            // save the status
            gm.hMgr.saveStatus();
            gm.hsMgr.saveStatus();
            
            gm.vMgr.saveStatus();
            gm.vsMgr.saveStatus();
            
            gm.eMgr.saveStatus();
        }
        catch(Exception e) {}
        
        
        
        
        
        
        /*
        // get the total time in case we exit out before
        // the time is up. then we can return with the
        // correct amount of time left.
        demoTotalTime = System.currentTimeMillis() - demoStartTime;
        
        loadLevelCounter = 0;
        
        loadSceneBool = true;
        loadSceneCounter = 0;
        
        playing = false;
        paused = true;
        pausedCounter = 0;
        
        // kill the load characters thread if it's running
        if(tLoadObj3d != null)
            tLoadObj3d.killThread();
        
        // kill the logic thread that is running then join it
        if(lThread != null)
            lThread.stopLogicThread();
        
        try{t.join();} catch(Exception e) {}
        */
    }
    
    // this is called from the GL thread instead of onResume(). the reason is
    // that it has problems loading the textures if (and setting up the scene)
    // if we do not call from the GL thread.
    public void initScene() {
        
        tLoadObj3d = new LoadCharacters(gm);
        gm.glSurfaceView.queueEvent(tLoadObj3d);
        
        // fire up the logic manager
        lThread = new LogicThread();
        t = new Thread(lThread);
        t.start();
    }
}
