package upe.projectv.game.levels;

import android.os.Message;
import android.util.Log;
import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.gameobjects.iShip;
import min3d.vos.Number3d;
import java.util.LinkedList;
import java.util.Iterator;
import upe.projectv.game.managers.objectmgmt.VillainShotManager;
import upe.projectv.game.statics.StaticWindow;
import java.util.Random;
import upe.projectv.game.managers.gamemgmt.GameManager;

public class Level2 implements iLevel {
    
    private GameManager gm;
    
    // these variables are for the background configuration
    
    // background picture
    //private final String backgroundPath = "background1png";  // bg pic name
    private final String backgroundPath = "background3png3";  // bg pic name
    private final int width = 2048;  // bg pic width
    private final int height = 1024;
    
    // position of the background 
    // the position in z terms of the background closest to the camera
    private final float zPosition = -70; 
    private final int configure = 2;
    private final int alignment = 1;
    private final int numberOfGroups = 2;
    
    private float[] villain0Info = new float[] {
        
        // front group
        -5,0,-30,0,
        5,0,-30,0,
            
        // second group
        0,0,-40,1,
        
        // third group
        -5,0,-50,0,
        5,0,-50,0,
            
        // fourth group
        0,0,-60,1
    };

    private float[] villain1Info = new float[] {
        
        // front group
        0,-1.5566038375f,-30,0,
            
        // second group
        -5,-1.5566038375f,-40,1,
        5,-1.5566038375f,-40,1,
        
        // third group
        0,-1.5566038375f,-50,0,
        
        // fourth group
        -5,-1.5566038375f,-60,1,
        5,-1.5566038375f,-60,1
    };
    
    // these variables are for tracking the movement
    private Iterator<iShip> vIterator;
    private iShip vTemp;
    //private float lbound = -50,rbound = 50;
    private float xbound = 50;
    private float moveby = 0.25f;
    private boolean changedirection = false;
    
    // these values are for the shooting AI
    
    // minimum number of cycles between villains shooting
    private int minShotCycles;
    // the variable number of cycles between villains shooting
    private int varShotCycles;
    // the number of cycles between shots (variable, based on above values)
    private int cyclesBetweenShots;
    // the counter for what cycle number we are on
    private int shotCycleCounter;
    // the number of cycles between when a villain is able to refire
    private int villainShotDelay;
    
    // the maximum number of regular shots before a hero shot
    private int maxShotsBetweenHeroShots;
    // above is randomized and changes after each hero shot. this is current
    private int currentShotsBetweenHeroShots;
    // this counts the number of shots so we know when to do a hero shot
    private int counterShotsBetweenHeroShots;
    // the number of villains that are able to fire
    private int villainsAbleToFire;
    // this array tracks which villains are able to fire
    private int[] villainTracker;
    
    // the number of villains able to fire directly at the hero
    private int villainsAbleToFireHero;
    // this array tracks the villains able to fire at the hero
    private int[] villainTrackerHero;
    
    // counts where we are in villainTracker and villainTrackerHero
    private int arrayCounter;
    
    private float heroShotSpeed = 0.6f;
    private float villainShotSpeed = 0.6f;
    
    // initialize everything
    public Level2(GameManager _gm) {
        
        minShotCycles = 30;
        varShotCycles = 30;
        cyclesBetweenShots = 0;
        shotCycleCounter = 0;
        villainShotDelay = 30;
        
        maxShotsBetweenHeroShots = 1;       // this should be >= 1
        currentShotsBetweenHeroShots = 0;   // >= 0
        counterShotsBetweenHeroShots = 0;   // >= 0
        
        // set the bounds for the x direction
        xbound = StaticWindow.getScreenWidthAtZ(-30);
        
        // set the shot speeds for the hero and villain
        _gm.hsMgr.setShotSpeed(heroShotSpeed);
        _gm.vsMgr.setShotSpeed(villainShotSpeed);
        this.gm = _gm;
        
        villainsAbleToFire = 0;
        villainTracker = new int[villain0Info.length];
        for(int i = 0; i < villainTracker.length; i++)
            villainTracker[i] = 0;
        
        villainsAbleToFireHero = 0;
        villainTrackerHero = new int[villain0Info.length];
        for(int i = 0; i < villainTrackerHero.length; i++)
            villainTrackerHero[i] = 0;
    }
    
    // this is the AI function for moving a villain. an interator moves through
    // the active bad guys and each one calls this function to process that
    // individual villain.
    private void villainMoveAi(iShip _vTemp,float _moveby,float _bound) {
        
        // we have two groups, so we do the opposite of each other
        Number3d n = _vTemp.getObject3dContainer().position();
        
        if(_vTemp.getGroupNumber() == 0)
            n.x += _moveby;
        else
            n.x -= _moveby;
            
        _vTemp.getObject3dContainer().position().setAllFrom(n);

        // if the boundaries have been exceeded, set the flag to switch
        // the movement
        if(n.x > _bound || n.x < -_bound)
            changedirection = true;
    }
    
    // an iterator goes through each villain at the time that villainMoveAi is
    // gone through. it calls this function for each ship to see if there it is
    // able to fire. it returns true
    private boolean villainCheckStandardShotAi
            (iShip _vTemp,float _villainShotDelay) {
        
        // check if the bad guy's x value is not too far
        if (_vTemp.getObject3dContainer().position().x
                > -StaticWindow.herox
                && _vTemp.getObject3dContainer().position().x
                < StaticWindow.herox) {

            // check the villain's shot count
            if (_vTemp.getShotDelayCount() > _villainShotDelay)
                return true;
            // if not clear him out
            else
                return false;
        } 
        // if not clear him out
        else
            return false;
    }
    
    // this function, like the one above, checks if the passed in villain
    // is eligible to fire. additionally, it checks if the villain is directly
    // in front of the hero
    private boolean villainCheckHeroShotAi
            (GameManager _gm,iShip _vTemp,float _villainShotDelay) {
        
        // check if the bad guy's x value is not too far
        if (_vTemp.getObject3dContainer().position().x
                > -StaticWindow.herox
                && _vTemp.getObject3dContainer().position().x
                < StaticWindow.herox) {

            // check the villain's shot count
            if (_vTemp.getShotDelayCount() > _villainShotDelay) {
                
                // now check if the villain is in front of the hero
                if(_vTemp.getObject3dContainer().position().x <= 
                        _gm.hMgr.getHero().getObject3dContainer().position().x 
                        + _gm.hMgr.getHero().getBoundingBox()[1]
                        && _vTemp.getObject3dContainer().position().x >= 
                        _gm.hMgr.getHero().getObject3dContainer().position().x 
                        + _gm.hMgr.getHero().getBoundingBox()[0]) {
                    
                    return true;
                }
                
                return false;
            }
            // if not clear him out
            else
                return false;
        } 
        // if not clear him out
        else
            return false;
    }
    
    
    // in the function above, we found what villains are able to fire. in this
    // function, we select a random one of the villains available to fire, and
    // we then fire.
    private iShip villainFireStandardShot(GameManager _gm,
            int _villainsAbleToFire,int[] _villainTracker,int _arrayCounter) {
        
        // choose a random number of the number of bad guys available
        // create random generator
        Random random = new Random();

        // generate a number from 0 villainsAbleToFire - 1
        int random1 = random.nextInt(_villainsAbleToFire);

        int counter = 0, i = 0;

        Iterator<iShip> _vIterator = _gm.vMgr.getActiveVillains().iterator();
        iShip _vTemp;

        // now go through the array of villains and count to the
        // villain corresponding to random1
        while (i < _arrayCounter && _vIterator.hasNext()) {

            _vTemp = _vIterator.next();

            // if the villain is one that is able to shoot
            if (_villainTracker[i] == 1) {
                
                // if this is not the villain who needs to shoot
                if (counter != random1)
                    counter += 1;
                // this is the villain that will shoot
                else
                    return _vTemp;
            }
            
            // increment i
            i += 1;
        }
        
        return null;
    }
    
    // in this function we look for a villain that is right in front of the
    // hero. we do this to make sure the hero can't sit in one spot for a 
    // long time.
    private iShip villainFireHeroShot(GameManager _gm,
            int _villainsAbleToFireHero,int[] _villainTrackerHero,
            int _arrayCounter) {
        
        // choose a random number of the number of bad guys available
        // create random generator
        Random random = new Random();

        // generate a number from 0 villainsAbleToFire - 1
        int random1 = random.nextInt(_villainsAbleToFireHero);

        int counter = 0, i = 0;

        Iterator<iShip> _vIterator = _gm.vMgr.getActiveVillains().iterator();
        iShip _vTemp;

        // now go through the array of villains and count to the
        // villain corresponding to random1
        while (i < _arrayCounter && _vIterator.hasNext()) {

            _vTemp = _vIterator.next();

            // if the villain is one that is able to shoot
            if (_villainTrackerHero[i] == 1) {
                
                // if this is not the villain who needs to shoot
                if (counter != random1)
                    counter += 1;
                // this is the villain that will shoot
                else
                    return _vTemp;
            }
            
            // increment i
            i += 1;
        }
        
        return null;
    }
    
    @Override
    public void introVillainAI(VillainManager vMgr) {
        
        // get the bad guys
        
        // iterate through the bad guys and move them
        vIterator = vMgr.getActiveVillains().iterator();
        
        while(vIterator.hasNext()) {
            
            vTemp = vIterator.next();
            
            villainMoveAi(vTemp,moveby,xbound);
        }
        
        // if we need to change direction, switch signs on moveby and reset
        // the changedirection flat
        if(changedirection) {
            
            moveby = -moveby;
            changedirection = false;
        }
    }
    
    @Override
    public void gameVillainAI(VillainManager vMgr,VillainShotManager vsMgr) {
        
        // set arrayCounter to 0
        arrayCounter = 0;
        
        // set villainsAbleToFire back to 0
        villainsAbleToFire = villainsAbleToFireHero = 0;
        
        // iterate through the bad guys and move them
        vIterator = vMgr.getActiveVillains().iterator();
        
        while(vIterator.hasNext()) {
            
            vTemp = vIterator.next();
            
            villainMoveAi(vTemp,moveby,xbound);
            
            // FOR SHOOTING AI
            // first check if it is time to check for a shot
            if (shotCycleCounter < cyclesBetweenShots) {}
            else {
                
                // if the villain is eligible to fire
                if(villainCheckStandardShotAi(vTemp,villainShotDelay)) {
                    
                    // add the villain to the array of villains able to shoot
                    villainTracker[arrayCounter] = 1;

                    // increment the villains able to shoot
                    villainsAbleToFire += 1;
                }
                // if not, clear him
                else
                    villainTracker[arrayCounter] = 0;
                
                // now check if the villain is eligible to fire AND is directly
                // in front of the hero
                if(villainCheckHeroShotAi(gm,vTemp,villainShotDelay)) {
                    
                    // add the villain to the array of villains able to shoot
                    villainTrackerHero[arrayCounter] = 1;

                    // increment the villains able to shoot
                    villainsAbleToFireHero += 1;
                }
                // if not, clear him
                else
                    villainTrackerHero[arrayCounter] = 0;
                
                // increment arrayCounter
                arrayCounter += 1;
            }
            
            // increment the villain's shot delaycount
            vTemp.setShotDelayCount(vTemp.getShotDelayCount() + 1);
        }
        
        // if we need to change direction, switch signs on moveby and reset
        // the changedirection
        if(changedirection) {
            
            moveby = -moveby;
            changedirection = false;
        }
        
        // if we need to fire off a shot
        if (shotCycleCounter < cyclesBetweenShots) {}
        else {
            
            // check if we have a villain able to shoot directly at the hero
            if(counterShotsBetweenHeroShots >= currentShotsBetweenHeroShots
                    && villainsAbleToFireHero > 0) {
                
                // if it's time to shoot a hero shot
                if( (vTemp = villainFireHeroShot(gm,villainsAbleToFireHero,
                        villainTrackerHero,arrayCounter)) == null) {}
                else {
                    
                    // Log.v("JOEL","Doing a hero shot");
                    // Log.v("JOEL","counter : " + counterShotsBetweenHeroShots);
                    // Log.v("JOEL","villains : " + villainsAbleToFireHero);
                    
                    // add a shot
                    vsMgr.addShot(vTemp.getObject3dContainer().position());
                    
                    Message shotMsg = Message.obtain();
                    shotMsg.arg1 = 201;
                    gm.overlayHandler.sendMessage(shotMsg);

                    // reset the cycle counter
                    shotCycleCounter = 0;

                    // reset the shot cycle counter for the villain
                    vTemp.setShotDelayCount(0);

                    Random random = new Random();
                    cyclesBetweenShots = minShotCycles 
                            + random.nextInt(varShotCycles);
                    
                    // now reset values for hero shots
                    counterShotsBetweenHeroShots = 0;
                    
                    // select a random number for next number of hero shots
                    currentShotsBetweenHeroShots =
                            random.nextInt(maxShotsBetweenHeroShots);
                }
            }
            
            // make sure there is at least one villain available to shoot
            // if not, do nothing. we'll get it next time around
            //if (villainsAbleToFire < 1) {} 
            else if (villainsAbleToFire > 0) {
                
                if((vTemp = villainFireStandardShot(gm,villainsAbleToFire,
                        villainTracker,arrayCounter)) == null) {}
                else {
                    
                    // Log.v("JOEL","Doing a regular shot");
                    
                    // add a shot
                    vsMgr.addShot(vTemp.getObject3dContainer().position());
                    
                    Message shotMsg = Message.obtain();
                    shotMsg.arg1 = 201;
                    gm.overlayHandler.sendMessage(shotMsg);

                    // reset the cycle counter
                    shotCycleCounter = 0;

                    // reset the shot cycle counter for the villain
                    vTemp.setShotDelayCount(0);

                    Random random = new Random();
                    cyclesBetweenShots = minShotCycles 
                            + random.nextInt(varShotCycles);
                    
                    // add to counterShotsBetweenHeroShots. this way will reach
                    // the limit and fire a direct hero shot
                    counterShotsBetweenHeroShots += 1;
                }
            }
            
        }
        
        // increase the shot cycle counter
        shotCycleCounter += 1;
    }
    
    @Override
    public String getBackgroundPath() {
        return backgroundPath;
    }
    @Override
    public int getBackgroundWidth() {
        return width;
    }
    @Override
    public int getBackgroundHeight() {
        return height;
    }
    @Override
    public float getBackgroundzPosition() {
        return zPosition;
    }
    @Override
    public int getBackgroundConfigure() {
        return configure;
    }
    @Override
    public int getBackgroundAlignment() {
        return alignment;
    }
    
    @Override
    public int getNumberOfGroups() {
        
        return numberOfGroups;
    }
    
    @Override
    public float[] getVillain0Info() {
        
        if(villain0Info.length % 4 != 0)
            return null;
        else
            return villain0Info;
    }
    
    @Override
    public float[] getVillain1Info() {
        
        if(villain1Info.length % 4 != 0) {
            // Log.v("JOEL","Villain 1 null");
            // Log.v("JOEL","Villain 1 length : " + villain1Info.length);
            return null;
        }
        else {
            // Log.v("JOEL","Villain 1 not null");
            return villain1Info;
        }
    }
}