package upe.projectv.game.levels;

import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.managers.objectmgmt.VillainShotManager;

public interface iLevel {
    
    // this is used to load the background
    //public void loadLevelBackground(BackgroundManager bMgr);
    public String getBackgroundPath();
    public int getBackgroundWidth();
    public int getBackgroundHeight();
    public float getBackgroundzPosition();
    public int getBackgroundConfigure();
    public int getBackgroundAlignment();
    
    public void introVillainAI(VillainManager vMgr);
    public void gameVillainAI(VillainManager vMgr,VillainShotManager vsMgr);
    
    // a group is a set of bad guys that move together
    public int getNumberOfGroups();
    
    // the villian info is the location of the villian along with its group
    // group numbers are from 0 to 3
    public float[] getVillain0Info();
    public float[] getVillain1Info();
    
}