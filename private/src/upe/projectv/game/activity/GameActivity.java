package upe.projectv.game.activity;




import android.os.Bundle;
import min3d.core.RendererActivity;
import upe.projectv.game.managers.gamemgmt.GameManager;
import android.view.MotionEvent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.opengl.GLSurfaceView;
import android.view.View.OnTouchListener;
import android.view.View;
import android.view.WindowManager;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.graphics.Typeface;
import android.graphics.Color;
import android.view.Gravity;
import android.media.MediaPlayer;

import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;

import android.util.Log;

import android.view.*;
import upe.projectv.game.statics.StaticWindow;
    
public class GameActivity extends RendererActivity
        implements SensorEventListener,OnTouchListener {

    private GameManager gm;
    
    //ERICECHE 
    private MediaPlayer mplayer,touchscreennoise,playerkilled;
    // for the overlay
    LinearLayout topbar,topbarleft,topbarmiddle,topbarright,
            leftspacer,rightspacer;
    RelativeLayout overlay, middlebar;
    FrameLayout container;
    ImageView livesImage,zunderImage;
    TextView livesText, livesValue, scoreText, scoreValue, middlemessage,
            levelText, levelValue;
    Typeface font;
    private Handler overlayHandler;
    private String scoreValueString ;
    private Vibrator vibrator;
    
    // for shake detection
    private SensorManager msensorManager;
    private Sensor magsensor,accsensor;
    private WindowManager wm;
    private float[] gravdata = new float[3];
    private float[] magdata = new float[3];
    private float[] Rotation = new float[16];
    private float[] Inclination = new float[16];
    private float[] Orientation = new float[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        
        super.onCreate(savedInstanceState);
        
        
        
        msensorManager = (SensorManager)getSystemService
                (Activity.SENSOR_SERVICE);
        magsensor = msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        accsensor = msensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		
		
        
        // get the width and height of the screen
         wm = getWindowManager();
        Display d = wm.getDefaultDisplay();
        
        // Log.v("JOEL-Window Width",String.valueOf(d.getWidth()));
        // Log.v("JOEL-Window Height",String.valueOf(d.getHeight()));
        
        // set the window width and height in pixels
        StaticWindow.window_width = d.getWidth();
        StaticWindow.window_height = d.getHeight();
        
        // run the function to calculate the near, far, etc. all values needed
        // are hard-coded from FrustumManaged class at this point, so we're ok
        StaticWindow.calculateScreenDimensions();
        
        overlayHandler = setOverlayHandler();
        
        gm = new GameManager(_glSurfaceView,scene,overlayHandler);
        
        // set the render mode to when dirty
        _glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        
        // set so that the volume in the app controls the media volume
        // instead of the ringer volume
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
   
        
        // ERIC ECHE Play the Music using raw mp3
        if (Global.settings[1])
            mplayer = MediaPlayer.create(this, R.raw.nullsleep);
        else
            mplayer = MediaPlayer.create(this, R.raw.silent);

        if (Global.settings[0])
            touchscreennoise = MediaPlayer.create(this, R.raw.laser_2);
        else
            touchscreennoise = MediaPlayer.create(this, R.raw.silent);

        if (Global.settings[0])
            playerkilled = MediaPlayer.create(this, R.raw.explosion);
        else
            playerkilled = MediaPlayer.create(this, R.raw.silent);

        mplayer.setLooping(false); // Set looping
        mplayer.start();


        initOverlay();

        // Log.v("JOEL","Finished onCreate");
    }
    
    @Override
    protected void onResume() {
        
        super.onResume();
        mplayer.start();
        _glSurfaceView.setOnTouchListener(this);
        msensorManager.registerListener(this,magsensor,
                SensorManager.SENSOR_DELAY_GAME);
        msensorManager.registerListener(this,accsensor,
                SensorManager.SENSOR_DELAY_GAME);
    }
	
    @Override
    protected void onPause() {
        super.onPause();
        mplayer.pause();
        msensorManager.unregisterListener(this);
        _glSurfaceView.setOnTouchListener(null);
        
        gm.onPause();
    }
    
    @Override
    protected void onDestroy() {
        
        super.onDestroy();
        mplayer.stop();
        XMLfunctions.saveScore(Global.playername,
                Integer.parseInt(scoreValueString));
        // Log.v("JOEL","Start onDestroy");
    }
    
    // where initialization occurs. is called everytime the app
    // resumes, so it is essentially onResume. Except that this
    // comes from the GL thread.
    @Override
    public void initScene() {
        gm.initScene();
    }
    
    // main rendering area (like in OpenGL)
    @Override
    public void updateScene() {
        //gm.updateScene();
    }
    
    // FOR SENSING ANDROID SHIFTING
    // SENSOR READINGS - ACCELEROMETER AND MAGNETIC FIELDS *********************
    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            System.arraycopy(event.values,0,gravdata,0,event.values.length);
        else if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            System.arraycopy(event.values,0,magdata,0,event.values.length);
        else 
            return;
        
        // if we're able to get the rotation matrix
        if(SensorManager.getRotationMatrix
                (Rotation,Inclination,gravdata,magdata)) {
            SensorManager.getOrientation(Rotation,Orientation);
            
            // sometimes an error occurs here (looks like hero or the
            // Object3dContainer is null) when reorienting screen. see the
            // notes for more (10).
            try {gm.androidShifted(Orientation[1]);}        // for Android
            //try {gm.androidShifted(Orientation[2]);}        // for Blackberry
            catch(Exception e) {}
        }
    }

    @Override
    public void onAccuracyChanged(Sensor s,int accuracy) {}
    
    // DETECT SCREEN TOUCH******************************************************
    @Override
    public boolean onTouch(View view,MotionEvent event) {
        
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            gm.screenTouched();
            
            //ERICECHE Play Shooting Sound when user touch the screen
            //touchscreennoise.start();
            return true;
        }
        return false;
    }
    
    //****************** SETUP THE SCREEN OVERLAY ******************************
    private Handler setOverlayHandler() {
        
        Handler temp = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                
                // Log.v("JOEL","got to handlemessage function");

                switch (msg.arg1) {

                    // this sets the value of the middle text (the one where loading
                    // etc. is)
                    case 100:
                        middlemessage.setText((String)msg.obj);
                        break;

                    // this sets the lives.
                    //If lives are set , must be guy got kill
                    //ERICECHE play the destroy sound
                    case 101:
                    	playerkilled.start();
                        livesValue.setText((String)msg.obj);
                        break;

                    // sets the level
                    case 102:
                        levelValue.setText((String)msg.obj);
                        break;

                    // sets the score
                    case 103:
                        scoreValue.setText((String)msg.obj);
                        scoreValueString = (String)msg.obj;
                        break;
                        
                    // this hides the overlay
                    case 104:
                        livesImage.setVisibility(ImageView.INVISIBLE);
                        livesText.setVisibility(TextView.INVISIBLE);
                        livesValue.setVisibility(TextView.INVISIBLE);
                        scoreText.setVisibility(TextView.INVISIBLE);
                        scoreValue.setVisibility(TextView.INVISIBLE);
                        middlemessage.setVisibility(TextView.INVISIBLE);
                        levelText.setVisibility(TextView.INVISIBLE);
                        levelValue.setVisibility(TextView.INVISIBLE);
                        break;
                        
                    // this displays the overlay
                    case 105:
                        livesImage.setVisibility(ImageView.VISIBLE);
                        livesText.setVisibility(TextView.VISIBLE);
                        livesValue.setVisibility(TextView.VISIBLE);
                        scoreText.setVisibility(TextView.VISIBLE);
                        scoreValue.setVisibility(TextView.VISIBLE);
                        middlemessage.setVisibility(TextView.VISIBLE);
                        levelText.setVisibility(TextView.VISIBLE);
                        levelValue.setVisibility(TextView.VISIBLE);
                        break;
                        
                    // this displays just the middletext
                    case 106:
                        middlemessage.setVisibility(TextView.VISIBLE);
                        break;
                        
                    // when the framelayout is initially loaded, it is hidden.
                    // this will make it visible
                    case 107:
                        overlay.setVisibility(RelativeLayout.VISIBLE);
                        break;
                        
                        
                        
                    // ALL THE AUDIO STUFF
                        
                    // play explosion
                    case 200:
                      	 if (Global.settings[2])
         	                  vibrator.vibrate(300);
                        break;
                        
                    // play shot fired
                    case 201:
                        touchscreennoise.start();
                        break;
                       
                        
                        
                        
                    // this is for when the game has finished playing
                    case 300:
                        // TODO - move to purchase screen
                        break;
                        
                }
            }
        };
        
        return temp;
    }
    
    public void initOverlay() {
        
        // set up the font
        font = Typeface.createFromAsset(getAssets(),"fonts/earth.ttf");
        
        // this will contain all the overlay stuff as well as the background
        container = new FrameLayout(this.getApplicationContext());
        container.setLayoutParams(new FrameLayout.LayoutParams
                (FrameLayout.LayoutParams.FILL_PARENT,
                FrameLayout.LayoutParams.FILL_PARENT));
        //container.setVisibility(FrameLayout.INVISIBLE);
        
        // set up LinearLayout containing the ship image, life number, score
        // in text, the actual score value, and the main messages
        overlay = new RelativeLayout(this.getApplicationContext());
        overlay.setLayoutParams(new RelativeLayout.LayoutParams
                (RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.FILL_PARENT));
        //overlay.setPadding(0,50,0,0);
        //overlay.setBackgroundColor(0xffffffff);
        overlay.setBackgroundColor(0x00000000);
        overlay.setVisibility(RelativeLayout.INVISIBLE);
        
        
        // set up LinearLayout containing the stuff at the top
        topbar = new LinearLayout(this.getApplicationContext());
        topbar.setLayoutParams(new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        //topbar.setGravity(Gravity.TOP);
        //topbar.setBackgroundColor(0xffff0000);
        topbar.setBackgroundColor(0x00ff0000);
        
        // set up LinearLayout containing the ship image and life number
        topbarleft = new LinearLayout(this.getApplicationContext());
        topbarleft.setLayoutParams(new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        topbarleft.setGravity(Gravity.CENTER_VERTICAL);
        //topbarleft.setBackgroundColor(0xffff0000);
        
        // imageview set up
        livesImage = new ImageView(this.getApplicationContext());
        livesImage.setImageResource(R.drawable.ship4);
        topbarleft.addView(livesImage);
        
        // the textview for the 'x' for the number of lives
        livesText = new TextView(this.getApplicationContext());
        livesText.setTypeface(font);
        livesText.setTextColor(Color.YELLOW);
        //livesText.setTextSize(40);
        livesText.setText(" x ");
        topbarleft.addView(livesText);
        
        // the textview for the number of lives set up
        livesValue = new TextView(this.getApplicationContext());
        livesValue.setTypeface(font);
        livesValue.setTextColor(Color.YELLOW);
        //livesValue.setTextSize(40);
        livesValue.setText("3");
        topbarleft.addView(livesValue);
        
        
        
        // this is the spacer between the number of lives and the 
        // current level
        leftspacer = new LinearLayout(this.getApplicationContext());
        LinearLayout.LayoutParams lpls = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lpls.weight = 1;
        //lpls.width = 20;
        leftspacer.setLayoutParams(lpls);
        //topbarmiddle.setGravity(Gravity.RIGHT);
        
        
        
        
        // set up LinearLayout containing the current level
        topbarmiddle = new LinearLayout(this.getApplicationContext());
        LinearLayout.LayoutParams lpm = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.FILL_PARENT);
        //lpm.weight = 1;
        topbarmiddle.setLayoutParams(lpm);
        topbarmiddle.setGravity(Gravity.CENTER_VERTICAL);
        //topbarmiddle.setGravity(Gravity.RIGHT);
        //topbarmiddle.setBackgroundColor(0xff00ff00);
        
        // the textview for the 'level' text
        levelText = new TextView(this.getApplicationContext());
        levelText.setTypeface(font);
        levelText.setTextColor(Color.YELLOW);
        //levelText.setTextSize(40);
        levelText.setText("Level : ");
        topbarmiddle.addView(levelText);
        
        // the textview for the current level
        levelValue = new TextView(this.getApplicationContext());
        levelValue.setTypeface(font);
        levelValue.setTextColor(Color.YELLOW);
        //levelValue.setTextSize(40);
        levelValue.setText("1");
        topbarmiddle.addView(levelValue);
        
        
        
        // this is the spacer between the number of lives and the 
        // current level
        rightspacer = new LinearLayout(this.getApplicationContext());
        LinearLayout.LayoutParams lprs = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lprs.weight = 1;
        rightspacer.setLayoutParams(lprs);
        //topbarmiddle.setGravity(Gravity.RIGHT);
        
        // set up LinearLayout containing the actual score value
        topbarright = new LinearLayout(this.getApplicationContext());
        LinearLayout.LayoutParams lptbr = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.FILL_PARENT);
        //lp.weight = 1;
        //lp.width = 200;
        topbarright.setLayoutParams(lptbr);
        //topbarright.setLayoutParams(new LinearLayout.LayoutParams
        //        (LinearLayout.LayoutParams.WRAP_CONTENT,
        //        LinearLayout.LayoutParams.WRAP_CONTENT));
        topbarright.setGravity(Gravity.CENTER_VERTICAL);
        //topbarright.setGravity(Gravity.RIGHT);
        //topbarright.setBackgroundColor(0xff0000ff);
        
        
        // the text for 'score : '
        scoreText = new TextView(this.getApplicationContext());
        scoreText.setTypeface(font);
        scoreText.setTextColor(Color.YELLOW);
        //scoreText.setTextSize(40);
        scoreText.setText("score : ");
        topbarright.addView(scoreText);
        
        // the text for the score
        scoreValue = new TextView(this.getApplicationContext());
        scoreValue.setTypeface(font);
        scoreValue.setTextColor(Color.YELLOW);
        //scoreValue.setTextSize(40);
        scoreValue.setText("100000");
        topbarright.addView(scoreValue);
        
        
        
        // set up the middle part where 'Get Ready' and 'Game Over' will be
        middlebar = new RelativeLayout(this.getApplicationContext());
        RelativeLayout.LayoutParams lpmb = new RelativeLayout.LayoutParams
                (RelativeLayout.LayoutParams.FILL_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        lpmb.addRule(RelativeLayout.CENTER_VERTICAL);
        
        //LinearLayout.LayoutParams lpmb = new LinearLayout.LayoutParams
        //        (LinearLayout.LayoutParams.WRAP_CONTENT,
        //        LinearLayout.LayoutParams.WRAP_CONTENT);
        //lp.weight = 1;
        //lp.width = 200;
        
        middlebar.setLayoutParams(lpmb);
        //topbarright.setLayoutParams(new LinearLayout.LayoutParams
        //        (LinearLayout.LayoutParams.WRAP_CONTENT,
        //        LinearLayout.LayoutParams.WRAP_CONTENT));
        middlebar.setGravity(Gravity.CENTER);
        
        //middlebar.setBackgroundColor(0xff00ff00);
        middlebar.setBackgroundColor(0x0000ff00);
        
        // the text for the score
        middlemessage = new TextView(this.getApplicationContext());
        middlemessage.setTypeface(font);
        middlemessage.setTextColor(Color.RED);
        middlemessage.setTextSize(40);
        //middlemessage.setText("Prepare yourself");
        //middlemessage.setText("level begin");
        middlebar.addView(middlemessage);
        
        
        
        // add the zunder wars background to test transparency
        // imageview set up
        zunderImage = new ImageView(this.getApplicationContext());
        zunderImage.setImageResource(R.drawable.zunder);
        
        
        
        
        topbar.addView(topbarleft);
        topbar.addView(leftspacer);
        topbar.addView(topbarmiddle);
        topbar.addView(rightspacer);
        topbar.addView(topbarright);
        overlay.addView(topbar);
        overlay.addView(middlebar);
        //container.addView(zunderImage);
        
        container.addView(_glSurfaceView);
        
        container.addView(overlay);
        //container.addView(middlebar);
        setContentView(container);
    }
}
