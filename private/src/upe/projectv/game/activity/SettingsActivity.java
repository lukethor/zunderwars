package upe.projectv.game.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends Activity implements OnClickListener {

    private TextView tv, tv1, tv2, tv3, tv4;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        Typeface tf = Typeface.createFromAsset(getAssets(), "data/fonts/androidnation.ttf");

        tv = (TextView) findViewById(R.id.sound);
        tv1 = (TextView) findViewById(R.id.music);
        tv2 = (TextView) findViewById(R.id.vibrate);
        tv3 = (TextView) findViewById(R.id.graphics);
        tv4 = (TextView) findViewById(R.id.user);

        tv.setTypeface(tf);
        tv1.setTypeface(tf);
        tv2.setTypeface(tf);
        tv3.setTypeface(tf);
        tv4.setTypeface(tf);

        tv.setOnTouchListener(new CustomTouchListener());
        tv1.setOnTouchListener(new CustomTouchListener());
        tv2.setOnTouchListener(new CustomTouchListener());
        tv3.setOnTouchListener(new CustomTouchListener());
        tv4.setOnTouchListener(new CustomTouchListener());

        tv.setOnClickListener(this);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);

        getSettings();

        setText();
    }

    private void setText() {

        tv.setText(Global.settings[0] ? "Sound on" : "Sound off");
        tv1.setText(Global.settings[1] ? "Music on" : "Music off");
        tv2.setText(Global.settings[2] ? "Vibrate on" : "Vibrate off");
        tv3.setText(Global.settings[3] ? "Graphics high" : "Graphics low");
    }

    private void getSettings() {
        
        SharedPreferences settings = 
                getSharedPreferences(FrogzillaSpalsh.PREFS_NAME, 0);

        Global.settings[0] = settings.getBoolean("sound", true);
        Global.settings[1] = settings.getBoolean("music", true);
        Global.settings[2] = settings.getBoolean("vibrate", true);
        Global.settings[3] = settings.getBoolean("graphics", true);

        if (Global.settings[1]) {
            Global.startMusic(this);
        } else {
            Global.stopMusic();
        }

    }

    @Override
    public void onClick(View v) {
        SharedPreferences settings = getSharedPreferences(FrogzillaSpalsh.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        switch (v.getId()) {
            case R.id.sound:
                editor.putBoolean("sound", !Global.settings[0]);
                break;
            case R.id.music:
                editor.putBoolean("music", !Global.settings[1]);
                break;
            case R.id.vibrate:
                editor.putBoolean("vibrate", !Global.settings[2]);
                break;
            case R.id.graphics:
                editor.putBoolean("graphics", !Global.settings[3]);
                break;
            case R.id.user:
                makeUserNameInputDialog();
                break;
        }

        editor.commit();

        getSettings();
        setText();
    }

    private void makeUserNameInputDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Username");
        alert.setMessage("Input a new username");

        // Set an EditText view to get user input 
        final EditText input = new EditText(this);
        input.setText(Global.playername);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                Toast.makeText(getApplicationContext(), value + " is saved in settings", Toast.LENGTH_LONG).show();

                SharedPreferences settings = getSharedPreferences(FrogzillaSpalsh.PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();

                editor.putString("player", value);
                editor.commit();
            }
        });

        alert.setNegativeButton("Cancel", 
                new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {}
        });

        alert.show();

    }
}