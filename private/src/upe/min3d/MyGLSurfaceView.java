package upe.min3d;

import android.opengl.GLSurfaceView;
import android.content.Context;
import android.util.AttributeSet;

/**
 *
 * @author jbomb
 * min3d has an issue where in GLSurfaceView in the swap
 * function it crashes if you hit home during loading
 * before the objects appear on the screen. this class is
 * to override that function and fix that issue.
 */
public class MyGLSurfaceView extends GLSurfaceView {
    
    public MyGLSurfaceView(Context context) {
        super(context);
    }

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    // this is where the error occurs. we'll do a try catch and
    // see if that is able to fix the problem
    
}
