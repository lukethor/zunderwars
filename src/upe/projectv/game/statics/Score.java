/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package upe.projectv.game.statics;

/**
 *
 * @author jbomb
 */

import java.util.concurrent.Semaphore;

// to make the GameCycle and Renderer thread-safe, we need a mutex. this class
// handles that. it is static so we don't have to make any significant 
// modifications to Renderer and RendererActivity
public class Score {
    
    private static Semaphore scoreSemaphore = new Semaphore(1);
    private static long score = 0;
    
    public static long getScore() {
        
        try {scoreSemaphore.acquire();}
        catch(Exception e) {
            return -1;
        }
        
        long temp = score;
        
        scoreSemaphore.release();
        
        return temp;
    }
    
    public static long increaseScore(long amount) {
        
        try {scoreSemaphore.acquire();}
        catch(Exception e) {
            return -1;
        }
        
        score += amount;
        
        long temp = score;
        
        scoreSemaphore.release();
        
        return temp;
    }
    
    public static void setScore(long _score) {
        
        try {scoreSemaphore.acquire();}
        catch(Exception e) {
            return;
        }
        
        score = _score;
        
        scoreSemaphore.release();
    }
}
