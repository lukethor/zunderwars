/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package upe.projectv.game.statics;

import min3d.core.Object3dContainer;
import min3d.core.Object3d;
import android.util.Log;
import min3d.vos.Number3d;
import min3d.core.Number3dBufferList;

/**
 *
 * @author jbomb
 */
public class ModelBounds {
    
    // for testing get the bounding box vertices
    public static void printBoundingBox(Object3dContainer o) {
        // FOR TESTING, GET THE BOUNDING BOXES
        
        // bounding box for hero
        //Object3dContainer o = hero.getObject3dContainer();
        
        // bounding box for saucer (badguy_1)
        //BadGuy_1 bg1 = new BadGuy_1();
        //bg1.setObject3dContainer(actInterface);
        //scene.addChild(bg1.getObject3dContainer());
        //Object3dContainer o = bg1.getObject3dContainer();
        
        float maxx = Float.NEGATIVE_INFINITY,maxy = Float.NEGATIVE_INFINITY,
                maxz = Float.NEGATIVE_INFINITY,
                minx = Float.POSITIVE_INFINITY,miny = Float.POSITIVE_INFINITY,
                minz = Float.POSITIVE_INFINITY;
        
        // go through each child of the object
        for(int i = 0; i < o.numChildren(); i++) {
            
            Object3d oChild = o.getChildAt(i);
            Number3dBufferList list = oChild.points();
            
            // Log.v("=== num children : " + list.size() + "===" Integer.toString(o.numChildren()));
            
            Number3d temp;
            for(int j = 0; j < list.size(); j++) {
                temp = list.getAsNumber3d(j);
                if(temp.x > maxx) {
                    maxx = temp.x;
                }
                if (temp.y > maxy) {
                    maxy = temp.y;
                }
                if (temp.z > maxz) {
                    maxz = temp.z;
                }

                if (temp.x < minx) {
                    minx = temp.x;
                }
                if (temp.y < miny) {
                    miny = temp.y;
                }
                if (temp.z < minz) {
                    minz = temp.z;
                }
            }
        }
        
        // Log.v("=== num children ===",Integer.toString(o.numChildren()));
        // Log.v("---- MINX ",Float.toString(minx));
        // Log.v("---- MINY ",Float.toString(miny));
        // Log.v("---- MINZ ",Float.toString(minz));
        // Log.v("---- MAXX ",Float.toString(maxx));
        // Log.v("---- MAXY ",Float.toString(maxy));
        // Log.v("---- MAXZ ",Float.toString(maxz));
    }
    
}
