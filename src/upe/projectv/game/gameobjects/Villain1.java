package upe.projectv.game.gameobjects;

import min3d.vos.Number3d;
import min3d.core.Object3dContainer;
import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.Shared;

public class Villain1 implements iShip {
    
    private Object3dContainer obj;
    
    private iExplosion explosion;
    
    private Number3d translate;
    private Number3d rotate;
    private Number3d scale;
    
    private float[] bounds;
    private float scalingFactor;
    
    // 0 = alive, 1 = dying, in scene, 2 = dying, out of scene, 3 = dead
    private int status;
    
    private int group;
    
    private int shotDelay;
    
    public Villain1() {
        
        scalingFactor = 0.55f;
        // min, max x : -2.4047434,3.8938525
        // min, max y : -0.21365905,6.4400744
        // min, max z : -3.4299815,3.3988795
        
        // min x, max x, min z, max z
        bounds = new float[] {-2.4047434f * scalingFactor,
            3.8938525f * scalingFactor,
            -3.4299815f * scalingFactor,
            3.3988795f * scalingFactor};
        
        // set default values 
        translate = new Number3d(0,0.0f,-50);
        //translate = new Number3d(0,-3.113207675f,-50);
        rotate = new Number3d(0,0,0);
        scale = new Number3d(scalingFactor,scalingFactor,scalingFactor);
        
        status = 0;
        
        shotDelay = 0;
    }
    
    @Override
    public void setObject3dContainer(Object3dContainer o) {
        
        obj = o.clone();

        obj.position().setAllFrom(translate);
        obj.rotation().setAllFrom(rotate);
        obj.scale().setAllFrom(scale);
    }
    
    @Override
    public iExplosion getExplosion() {
        return explosion;
    }
    
    @Override
    public void setExplosion(iExplosion explosion) {
        this.explosion = explosion;
    }
    
    // before going into pause, we need to save the current status of the
    // 3D object (position, rotation, etc.)
    @Override
    public void saveStatus() {
        translate.setAllFrom(obj.position());
        rotate.setAllFrom(obj.rotation());
    }
    
    @Override
    public Object3dContainer getObject3dContainer() {
        return obj;
    }
    
    @Override
    public void setShipTranslation(Number3d n) {
        translate.setAllFrom(n);
    }
    
    @Override
    public void setObjTranslation(Number3d n) {
        obj.position().setAllFrom(n);
    }
    
    @Override
    public float[] getBoundingBox() {
        return bounds;
    }
    
    @Override
    public Number3d getLocation() {
        return obj.position();
    }
    
    @Override
    public void setStatus(int status) {
        
        this.status = status;
    }
    
    @Override
    public int getStatus() {
        
        return status;
    }
    
    @Override
    public void setGroupNumber(int group) {
        this.group = group;
    }
    
    @Override
    public int getGroupNumber() {
        return group;
    }
    
    @Override
    public int getShotDelayCount() {
        
        return shotDelay;
    }
    
    @Override
    public void setShotDelayCount(int shotDelay) {
        
        this.shotDelay = shotDelay;
    }
    
    @Override
    public int getType() {
        return 1;
    }
}
