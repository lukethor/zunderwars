package upe.projectv.game.gameobjects;

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;

public interface iShot {
    
    void setObject3dContainer(Object3dContainer o);
    void saveStatus();
    Object3dContainer getObject3dContainer();
    void setTranslation(Number3d n);
    void setTranslation(float x,float y,float z);
    float[] getBoundingBox();
    Number3d getLocation();
    void gotHit();
    int getLifeStatus();
}