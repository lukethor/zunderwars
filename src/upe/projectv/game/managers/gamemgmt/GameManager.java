package upe.projectv.game.managers.gamemgmt;

import min3d.core.Scene;
import min3d.Shared;
import java.util.LinkedList;

import upe.projectv.game.managers.gamemgmt.LoadCharacters;
import upe.projectv.game.managers.gamemgmt.LoadLevel;
import java.util.Iterator;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import min3d.vos.Number3d;
import upe.projectv.game.managers.objectmgmt.HeroShotManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import android.util.Log;
import upe.projectv.game.managers.objectmgmt.HeroManager;
import upe.projectv.game.managers.objectmgmt.CollisionManager;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import upe.projectv.game.levels.iLevel;
import upe.projectv.game.levels.Level0;
import upe.projectv.game.levels.Level1;

import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import android.opengl.GLSurfaceView;
import java.util.concurrent.Semaphore;
import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.managers.objectmgmt.CollisionManager;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import upe.projectv.game.managers.objectmgmt.HeroManager;
import upe.projectv.game.managers.objectmgmt.HeroShotManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.managers.objectmgmt.VillainShotManager;
import min3d.core.Scene;

import android.os.Handler;

public class GameManager {
    
    // passed in values
    protected GLSurfaceView glSurfaceView;
    public Scene scene;
    
    private Semaphore pauseSemaphore;
    private int touchQueueCounter;
    private Semaphore touchSemaphore;
    private float motionFloat;
    private Semaphore motionSemaphore;
    
    
    public HeroManager hMgr;
    //public Scene scene;
    public HeroShotManager hsMgr;
    public VillainManager vMgr;
    public VillainShotManager vsMgr;
    public CollisionManager cMgr;
    public ExplosionManager eMgr;
    public BackgroundManager backMgr;
    public Handler overlayHandler;
    public GameCycle gameCycle;
    
    public GameManager(GLSurfaceView glSurfaceView,Scene scene,
            Handler overlayHandler) {
        
        // set inputs to local values
        this.glSurfaceView = glSurfaceView;
        this.scene = scene;
        this.overlayHandler = overlayHandler;
        
        // initialize the managers
        hsMgr = new HeroShotManager(scene);
        eMgr = new ExplosionManager(scene);
        //hMgr = new HeroManager(scene, hsMgr, eMgr);
        hMgr = new HeroManager(this,scene);
        vMgr = new VillainManager(scene, eMgr);
        vsMgr = new VillainShotManager(scene);
        //cMgr = new CollisionManager(vMgr, vsMgr, hMgr, hsMgr, eMgr);
        cMgr = new CollisionManager(this);
        backMgr = new BackgroundManager(scene);
        gameCycle = new GameCycle(this,overlayHandler);
        
        pauseSemaphore = new Semaphore(1);
        touchQueueCounter = 0;
        touchSemaphore = new Semaphore(1);
        motionFloat = 0.0f;
        motionSemaphore = new Semaphore(1);
    }
    
    public boolean getOnPauseSemaphore() {
        
        // get the semaphore
        try {
            pauseSemaphore.acquire();
            return true;
        }
        catch(Exception e) {
            // Log.v("JOEL","Pause semaphore not acquired getOnPauseSemaphore");
            return false;
        }
    }
    
    public void releaseOnPauseSemaphore() {
        
        pauseSemaphore.release();
    }
    
    // called by the Main Activity when it's paused. this resets
    // the necessary values and closes down the threads if they're running
    public void onPause() {
            
        
        gameCycle.onPause();
        
        /*
        // get the semaphore
        try {pauseSemaphore.acquire();}
        catch(Exception e) {
            // Log.v("JOEL","Pause semaphore not paused");
            return;
        }
        
        gameCycle.onPause();
        
        // bug from Eric's phone and my new phone
        // I think when game opens, it has to shift to portrait mode. when this
        // happens if hero and shots are not initialized when it tries to save
        // it gets a null pointer exception. so do a try catch and if it 
        // catches an error, set needInit to true so it will reload from scratch
        // when it gets to portrait mode.
        try {
            // save the status
            hMgr.saveStatus();
            hsMgr.saveStatus();
            
            vMgr.saveStatus();
            vsMgr.saveStatus();
            
            eMgr.saveStatus();
        }
        catch(Exception e) {}
        
        pauseSemaphore.release();
        */
    }
    
    
    public void initScene() {
        
        // get the semaphore
        try {pauseSemaphore.acquire();}
        catch(Exception e) {
            // Log.v("JOEL","Resume semaphore not gotten");
            return;
        }
        
        gameCycle.initScene();
        
        pauseSemaphore.release();
    }
    
    public void screenTouched() {
        
        try {
            touchSemaphore.acquire();
            touchQueueCounter += 1;
            touchSemaphore.release();
        }
        catch(Exception e) {
            touchSemaphore.release();
        }
    }
    
    // when this is called, it returns 1 or 0, not a number greater than 1
    // of shots. the queue is then completely cleared out.
    public int getShotsFromQueue() {
        
        try {
            touchSemaphore.acquire();
            
            if(touchQueueCounter < 1) {
                touchSemaphore.release();
                return 0;
            }
            
            touchQueueCounter = 0;
            touchSemaphore.release();
            return 1;
        }
        catch(Exception e) {
            touchSemaphore.release();
        }
        
        return 0;
    }
    
    public void androidShifted(float angle) {
        
        // add the float to the motion queue
        try {
            
            motionSemaphore.acquire();
            
            motionFloat = angle;        // for Android
            //motionFloat = -angle;       // for Blackberry
            
            motionSemaphore.release();
        }
        catch(Exception e) {
            motionSemaphore.release();
        }
    }
    
    public float getHeroMovement() {
        
        try {
            
            motionSemaphore.acquire();
            
            float temp = motionFloat;
            
            motionSemaphore.release();
            
            return temp;
        }
        catch(Exception e) {
            motionSemaphore.release();
        }
        
        return Float.NaN;
    }
    
}