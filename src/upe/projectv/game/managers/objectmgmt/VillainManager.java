package upe.projectv.game.managers.objectmgmt;

import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import min3d.core.Scene;
import upe.projectv.game.gameobjects.Villain0;
import upe.projectv.game.gameobjects.Villain1;
import java.util.LinkedList;
import java.util.Iterator;
import upe.projectv.game.gameobjects.iShip;
import min3d.core.Object3dContainer;
import min3d.vos.Number3d;
import android.util.Log;
import upe.projectv.game.gameobjects.iExplosion;

import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.Shared;

public class VillainManager {
    
    private Scene scene;
    private ExplosionManager eMgr;
    
    private LinkedList<iShip> activeVillains;
    private LinkedList<iShip> dyingVillains0;
    private LinkedList<iShip> dyingVillains1;
    private LinkedList<iShip> inactiveVillains0;
    private LinkedList<iShip> inactiveVillains1;
    
    private Object3dContainer template0;
    private Object3dContainer template1;
    private Iterator<iShip> villainIterator;
    
    private final String modelPathVillain0 =
            "upe.projectv.game.activity:raw/platillo_volador_07";
    private final String modelPathVillain1 =
            "upe.projectv.game.activity:raw/ufo3a";
    
    public VillainManager(Scene scene,ExplosionManager eMgr) {
        
        this.scene = scene;
        this.eMgr = eMgr;
        
        // create the linked lists for the shots
        activeVillains = new LinkedList<iShip>();
        dyingVillains0 = new LinkedList<iShip>();
        dyingVillains1 = new LinkedList<iShip>();
        inactiveVillains0 = new LinkedList<iShip>();
        inactiveVillains1 = new LinkedList<iShip>();
        
        iShip tempVillain;
        
        for(int i = 0; i < 14; i++) {
            tempVillain = new Villain0();
            inactiveVillains0.addLast(tempVillain);
        }
        
        for(int i = 0; i < 14; i++) {
            tempVillain = new Villain1();
            inactiveVillains1.addLast(tempVillain);
        }
    }
    
    // this is used when finishing a level. it puts the active, dying0, and
    // dying1 queues back into the appropriate inactive queue
    public void emptyNonActiveQueues() {
        
        iShip tempVillain;
        
        // empty the active queue
        villainIterator = activeVillains.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
	    // Log.v("JOEL","VillainManager remove 1");
            villainIterator.remove();
            scene.removeChild(tempVillain.getObject3dContainer());
            
            if(tempVillain.getType() == 0)
                inactiveVillains0.add(tempVillain);
            if(tempVillain.getType() == 1)
                inactiveVillains1.add(tempVillain);
        }
        
        // empty the dying0 queue
        villainIterator = dyingVillains0.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
    	    // Log.v("JOEL","VillainManager remove 2");
            villainIterator.remove();
            scene.removeChild(tempVillain.getObject3dContainer());
            
            if(tempVillain.getType() == 0)
                inactiveVillains0.add(tempVillain);
            if(tempVillain.getType() == 1)
                inactiveVillains1.add(tempVillain);
            //inactiveVillains0.add(tempVillain);
        }
        
        // empty the dying1 queue
        villainIterator = dyingVillains1.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
    	    // Log.v("JOEL","VillainManager remove 3");
            villainIterator.remove();
            scene.removeChild(tempVillain.getObject3dContainer());
            
            if(tempVillain.getType() == 0)
                inactiveVillains0.add(tempVillain);
            if(tempVillain.getType() == 1)
                inactiveVillains1.add(tempVillain);
            //inactiveVillains0.add(tempVillain);
        }
    }
    
    // this function goes through the active, dying0, and dying1 villains and 
    // saves their current status
    public void saveStatus() {
        
        iShip tempVillain;
        
        // empty the active queue
        villainIterator = activeVillains.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            tempVillain.saveStatus();
        }
        
        // empty the dying0 queue
        villainIterator = dyingVillains0.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            tempVillain.saveStatus();
        }
        
        // empty the dying1 queue
        villainIterator = dyingVillains1.iterator();
        
        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            tempVillain.saveStatus();
        }
    }
    
    public void loadVillainIntoLevel(Number3d pos,int group,int type) {
        
        //iShip tempVillain = inactiveVillains0.remove();
        iShip tempVillain = null;
        if (type == 0)
            tempVillain = inactiveVillains0.remove();
        if (type == 1)
            tempVillain = inactiveVillains1.remove();

        tempVillain.setShipTranslation(pos);
        tempVillain.setObjTranslation(pos);
        tempVillain.setGroupNumber(group);
        tempVillain.setShotDelayCount(0);
        activeVillains.add(tempVillain);
        
        // make sure we're not adding a child twice
        if(scene.getChildIndexOf(tempVillain.getObject3dContainer()) < 0)
            scene.addChildAt(tempVillain.getObject3dContainer(),0);
        
        // Log.v("JOEL","bg loadObject");
    }
    
    // this is used when the villain has just been hit and needs to
    // be placed into the dying0 queue
    public void setVillainToDying(iShip ship) {
        
        dyingVillains0.add(ship);
        ship.setExplosion(eMgr.getExplosion(ship));
    }
    
    public Object3dContainer loadObject3dContainer(String path) {
        
        // load the 3d model
        IParser parser = Parser.createParser(Parser.Type.MAX_3DS,
                Shared.context().getResources(),path,false);
        parser.parse();
        Object3dContainer obj = parser.getParsedObject();
        return obj;
    }
    
    // this function is used to load up the object3dcontainer
    public void setObject3dContainers() {
        
        iShip tempVillain;
        
        // set up the template objects
        template0 = loadObject3dContainer(modelPathVillain0);
        template1 = loadObject3dContainer(modelPathVillain1);
        
        //upe.projectv.game.statics.ModelBounds.printBoundingBox(template1);
        
        // iterate through the active bad guys and set their
        // 3d object containers and then add them back to the scene
        villainIterator = activeVillains.iterator();

        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            
            if(tempVillain.getType() == 0)
                tempVillain.setObject3dContainer(template0);
            if(tempVillain.getType() == 1)
                tempVillain.setObject3dContainer(template1);
            //tempVillain.setObject3dContainer(template0);
            
            // make sure we're not adding a child twice
            if(scene.getChildIndexOf(tempVillain.getObject3dContainer()) < 0)
                scene.addChildAt(tempVillain.getObject3dContainer(),0);
        }
        
        // iterate through the dyingVillains0 and set their
        // 3d object containers and then add them back to the scene
        villainIterator = dyingVillains0.iterator();

        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            //tempVillain.setObject3dContainer(template0);
            if(tempVillain.getType() == 0)
                tempVillain.setObject3dContainer(template0);
            if(tempVillain.getType() == 1)
                tempVillain.setObject3dContainer(template1);
            
            // make sure we're not adding a child twice
            if(scene.getChildIndexOf(tempVillain.getObject3dContainer()) < 0)
                scene.addChildAt(tempVillain.getObject3dContainer(),0);
        }
        
        // iterate through the dyingVillains1 and just set their
        // 3d object containers; don't add them back to the scene
        villainIterator = dyingVillains1.iterator();

        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            //tempVillain.setObject3dContainer(template0);
            if(tempVillain.getType() == 0)
                tempVillain.setObject3dContainer(template0);
            if(tempVillain.getType() == 1)
                tempVillain.setObject3dContainer(template1);
        }
        
        // iterate through the inactive bad guys and just set their
        // 3d object containers
        villainIterator = inactiveVillains0.iterator();

        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            tempVillain.setObject3dContainer(template0);
        }
        
        villainIterator = inactiveVillains1.iterator();

        while(villainIterator.hasNext()) {
            
            tempVillain = villainIterator.next();
            tempVillain.setObject3dContainer(template1);
        }
    }
    
    // this just requires cycling through the dying0 and dying1 villains.
    // the movement of the actives is driven by the AI in the level classes
    // and the inactives don't move at all.
    public void cycle() {
        
        // the active villains are handled by the level
        
        iShip tempVillain;
        iExplosion explosion;
        int status;
        
        // iterate through the dying0 villains
        villainIterator = dyingVillains0.iterator();
        
        while(villainIterator.hasNext()) {
            tempVillain = villainIterator.next();
            
            explosion = tempVillain.getExplosion();
            
            status = explosion.cycle();
            
            // it's time to remove the villain from the scene
            if(status == 1) {
                
                // remove the ship from the scene
                scene.removeChild(tempVillain.getObject3dContainer());
                
                // move the villain to dying1
       		// Log.v("JOEL","VillainManager remove 4");
                villainIterator.remove();
                dyingVillains1.add(tempVillain);
            }
        }
        
        // iterate through the dying1 villains
        villainIterator = dyingVillains1.iterator();
        
        while(villainIterator.hasNext()) {
            tempVillain = villainIterator.next();
            
            explosion = tempVillain.getExplosion();
            
            status = explosion.cycle();
            
            // the explosion is done, time to finish up
            if(status == 2) {
                
                // put the villain back into the inactive queue
                // Log.v("JOEL","VillainManager remove 5");
                villainIterator.remove();
                //inactiveVillains0.add(tempVillain);
                if (tempVillain.getType() == 0)
                    inactiveVillains0.add(tempVillain);
                if (tempVillain.getType() == 1)
                    inactiveVillains1.add(tempVillain);
                
                // finish up the explosion
                eMgr.explosionFinished(explosion);
                // set the pointer to null
                explosion = null;
                tempVillain.setExplosion(null);
            }
        }
    }
    
    public LinkedList<iShip> getActiveVillains() {
        return this.activeVillains;
    }
    
    public LinkedList<iShip> getInactiveVillains0() {
        return this.inactiveVillains0;
    }
}
