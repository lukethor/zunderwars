package upe.projectv.game.managers.objectmgmt;

import android.os.Message;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import android.util.Log;
import min3d.Shared;
import upe.projectv.game.gameobjects.iShot;
import min3d.core.Scene;
import upe.projectv.game.gameobjects.Hero;
import min3d.vos.FrustumManaged;
import min3d.vos.Number3d;
import upe.projectv.game.gameobjects.iShip;
import min3d.core.Object3dContainer;
import upe.projectv.game.gameobjects.iExplosion;
import upe.projectv.game.managers.gamemgmt.GameManager;

import min3d.parser.IParser;
import min3d.parser.Parser;
import min3d.Shared;
import upe.projectv.game.statics.StaticWindow;

public class HeroManager {
    
    private final String modelPathHero =
            "upe.projectv.game.activity:raw/nave_espacial_03";
    
    private Object3dContainer template;
    
    private GameManager gm;
    private Scene scene;
    private HeroShotManager hsMgr;
    private ExplosionManager eMgr;
    private iShip hero;
    
    private int heroRespawnCounter;
    private int heroRespawnSubCounter;
    private int heroRespawnMax = 60;
    
    private int lives = 3;
    
    public HeroManager(GameManager gm,Scene scene) {
        
        this.scene = scene;
        this.hsMgr = gm.hsMgr;
        this.eMgr = gm.eMgr;
        this.gm = gm;
        
        hero = new Hero();
    }
    
    /*
    public HeroManager(Scene scene,HeroShotManager hsMgr,
            ExplosionManager eMgr) {
        this.scene = scene;
        this.hsMgr = hsMgr;
        this.eMgr = eMgr;
        
        hero = new Hero();
    }
    */
    
    public void clearActiveQueues() {
            
        scene.removeChild(hero.getObject3dContainer());
    }
    
    public void setHeroToDying() {
        
        //hero.setExplosion(eMgr.getExplosion(hero));
        hero.setExplosion(eMgr.getHeroExplosion(hero));
        hero.setStatus(1);
    }
    
    // this is called by the LoadLevel thread. each object that needs to be
    // in use will be pulled from the inactive to the active, given the 
    // Number3d for the position
    public void loadShipIntoLevel(Number3d n) {
        
        hero.setShipTranslation(n);
        hero.setObjTranslation(n);
        
        // make sure we're not adding a child twice
        if(scene.getChildIndexOf(hero.getObject3dContainer()) < 0)
            scene.addChildAt(hero.getObject3dContainer(),0);
    }
    
    // this function is used to load up the object3dcontainer for active shots
    public void setObject3dContainers() {
        
        // load the 3d model
        IParser parser = Parser.createParser(Parser.Type.MAX_3DS,
                Shared.context().getResources(),modelPathHero,false);
        parser.parse();
        template = parser.getParsedObject();
        
        hero.setObject3dContainer(template);
        
        // add the hero to the scene if he is in the right status
        if(hero.getStatus() == 0 || hero.getStatus() == 1)
            scene.addChild(hero.getObject3dContainer());
        
        // not an object 3d container but it each time the scene is reloaded
        // we'll get the screen parameters so we know if the good guy has gone
        // off of the screen. I believe that this should be okay because the
        // only way it could be thrown off is if the screen is resized when
        // we haven't paused the application, which doesn't happen.
        StaticWindow.herox = StaticWindow.getScreenWidthAtZ
                (hero.getObject3dContainer().position().z);
        
        Log.v("JOEL","herox : " + StaticWindow.herox);
    }
    
    // this just requires cycling through the dying0 and dying1 villains.
    // the movement of the actives is driven by the AI in the level classes
    // and the inactives don't move at all.
    public void cycle() {
        
        // status for the hero
        // status 0 : alive and well, 1 : dying, still in scene
        // 2 : dying, not in scene, 3 : respawn, 4 : dead, game over
        
        // if the hero is dying, keep the explosion going
        if(hero.getStatus() == 1 || hero.getStatus() == 2) {
        
            int status = hero.getExplosion().cycle();
            
            // this means we remove the hero from the scene
            if(status == 1) {
                scene.removeChild(hero.getObject3dContainer());
                hero.setStatus(2);
                
                // if we're dead, reset the hero's position and rotation
                hero.getObject3dContainer().position().x = 0;
                hero.getObject3dContainer().rotation().z = 0;
            }
            // we're all done, time to wrap up
            else if(status == 2) {
                // finish up the explosion
                eMgr.explosionFinished(hero.getExplosion());
                // set the pointer to null
                hero.setExplosion(null);
                
                // subtract a life
                lives -= 1;
                
                // change the lives
                Message msg = Message.obtain();
                msg.arg1 = 101;
                msg.obj = String.valueOf(lives);
                gm.overlayHandler.sendMessage(msg);
                
                // if lives is 0, it's game over
                if(lives == 0) {
                    
                    hero.setStatus(4);
                }
                // otherwise, do the respawn
                else {
                    // check here whether we're dead for good or need to respawn
                    hero.setStatus(3);

                    // set the counters to 0
                    heroRespawnCounter = 0;
                    heroRespawnSubCounter = 0;

                    // put the hero back into the scene
                    scene.addChild(hero.getObject3dContainer());

                    // put the hero back to original position
                    hero.getObject3dContainer().position().x = 0;
                    hero.getObject3dContainer().rotation().z = 0;
                    hero.getObject3dContainer().rotation().z = 0;
                }
            }
        }
        
        
        
        
        // if the hero is in a respawn
        else if(hero.getStatus() == 3) {
            
            // if we've reached the max count we're done
            if(heroRespawnCounter == heroRespawnMax) {
                
                // the hero is back to normal
                hero.setStatus(0);
                
                // if the hero is not in the scene, add him back
                if(scene.getChildIndexOf(hero.getObject3dContainer()) < 0)
                    scene.addChildAt(hero.getObject3dContainer(),0);
            }
            else {
                
                if(heroRespawnSubCounter == 3)
                    scene.removeChild(hero.getObject3dContainer());
                else if(heroRespawnSubCounter == 4) {
                    scene.addChild(hero.getObject3dContainer());
                    heroRespawnSubCounter = 0;
                }
                
                heroRespawnCounter += 1;
                heroRespawnSubCounter += 1;
            }
        }
    }
    
    // this function goes through the active shots and saves their current
    // status
    public void saveStatus() {
        hero.saveStatus();
    }
    
    public void addShot() {
        
        Number3d n = new Number3d();
        n.setAllFrom(hero.getObject3dContainer().position());
        hsMgr.addShot(n);
        
        // add the sound
        //SoundHandler.playLaserSound(); ********************************************************************
    }
    
    public void androidShifted(float angle) {
        
        if(angle == Float.NaN)
            return;
        
        // change the angle regardless
        hero.getObject3dContainer().rotation().z = (float)
                ((((Math.floor(angle * 
                (float)(180.0f/Math.PI))) / 90.0f) * 55.0f));
        
        // calculate if the position will be too far
        double tempx = hero.getObject3dContainer().position().x +
                (-(((Math.floor(angle
                * (float)(180.0f/Math.PI))) / 90.0f) * 2.0f));
        
        // make sure the hero isn't too far left or right
        if(tempx < -StaticWindow.herox) {
            hero.getObject3dContainer().position().x = -StaticWindow.herox;
            return;
        }
        else if(tempx > StaticWindow.herox) {
            hero.getObject3dContainer().position().x = StaticWindow.herox;
            return;
        }
        
        // if the hero hasn't moved too far adjust his position
        hero.getObject3dContainer().position().x = (float)tempx;
    }
    
    public void recenter() {
        
        if(hero.getObject3dContainer().position().x == 0)
            return;
        
        // if we're to the left of 0
        if(hero.getObject3dContainer().position().x < 0) {
            
            float angle = -0.2f;

            // change the angle regardless
            hero.getObject3dContainer().rotation().z = (float) 
                    ((((Math.floor(angle * (float)(180.0f / Math.PI))) / 90.0f) 
                    * 55.0f));

            // calculate if the position will be too far
            double tempx = hero.getObject3dContainer().position().x
                    + (-(((Math.floor(angle
                    * (float) (180.0f / Math.PI))) / 90.0f) * 2.0f));

            // make sure the hero isn't too far left or right
            if (tempx < 0 && hero.getObject3dContainer().position().x > 0) {
                hero.getObject3dContainer().position().x = 0;
                hero.getObject3dContainer().rotation().z = 0;
                return;
            } //else if(tempx > screenHalfWidth) {
            else if (tempx > 0 && hero.getObject3dContainer().position().x < 0) {
                hero.getObject3dContainer().position().x = 0;
                hero.getObject3dContainer().rotation().z = 0;
                return;
            }

            // if the hero hasn't moved too far adjust his position
            hero.getObject3dContainer().position().x = (float) tempx;
        }
        // if we're to the right of 0
        else {

            float angle = 0.2f;

            // change the angle regardless
            hero.getObject3dContainer().rotation().z = (float) 
                    ((((Math.floor(angle * (float)(180.0f / Math.PI))) / 90.0f) 
                    * 55.0f));

            // calculate if the position will be too far
            double tempx = hero.getObject3dContainer().position().x
                    + (-(((Math.floor(angle
                    * (float) (180.0f / Math.PI))) / 90.0f) * 2.0f));

            // make sure the hero isn't too far left or right
            if (tempx < 0 && hero.getObject3dContainer().position().x > 0) {
                hero.getObject3dContainer().position().x = 0;
                hero.getObject3dContainer().rotation().z = 0;
                return;
            } //else if(tempx > screenHalfWidth) {
            else if (tempx > 0 && hero.getObject3dContainer().position().x < 0) {
                hero.getObject3dContainer().position().x = 0;
                hero.getObject3dContainer().rotation().z = 0;
                return;
            }

            // if the hero hasn't moved too far adjust his position
            hero.getObject3dContainer().position().x = (float) tempx;
        }
    }
    
    public void setLives(int lives) {
        this.lives = lives;
    }
    
    public iShip getHero() {
        return hero;
    }
}
