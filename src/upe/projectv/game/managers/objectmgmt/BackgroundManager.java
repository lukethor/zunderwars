package upe.projectv.game.managers.objectmgmt;

import android.graphics.Bitmap;
import min3d.Utils;
import min3d.Shared;
import min3d.core.Scene;
import min3d.vos.FrustumManaged;
import android.R;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.content.Context;
import android.opengl.GLUtils;

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import min3d.vos.TextureVo;
import min3d.vos.TexEnvxVo;
import javax.microedition.khronos.opengles.GL10;
import android.util.Log;
import upe.projectv.game.statics.StaticWindow;


public class BackgroundManager {
    
    private Scene scene;
    private float surfaceAspectRatio;
    private int[] textures;
    
    private TextureVo texVo;
    private TexEnvxVo texEnv;
    private Object3dContainer obj;
    
    private final String path = "upe.projectv.game.activity:drawable/";
    private String texId;
    private int imgWidth;
    private int imgHeight;
    private float zPosition;
    private int configure;
    private int alignment;
    
    public BackgroundManager(Scene scene) {
        
        this.scene = scene;
    }
    
    public void emptyActiveQueue() {
        
        if(obj != null) {
            scene.removeChild(obj);
            obj.clear();
        }
        
        if(texId != null) {
            Shared.textureManager().deleteTexture(texId);
        }
        
        texVo = null;
        texEnv = null;
    }
    
    public void setBackgroundParameters(String texId,int imgWidth,
            int imgHeight,float zPosition,int configure,int alignment) {
        
        this.texId = texId;
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
        this.zPosition = zPosition;
        this.configure = configure;
        this.alignment = alignment;
        
        // Log.v("JOEL","bgpath : " + this.texId);
        // Log.v("JOEL","bgwidth : " + this.imgWidth);
        // Log.v("JOEL","bgheight : " + this.imgHeight);
        // Log.v("JOEL","bg z : " + this.zPosition);
        // Log.v("JOEL","bgconfig : " + this.configure);
        // Log.v("JOEL","bgalign : " + this.alignment);
    }
    
    
    // this function calculates how to make the background fit into the whole
    // screen. it is angled, like the camera, and unlike the ships and shots.
    // if minor variations are made to the angle, etc., this should work. this
    // will NOT work if major changes occur.
    // backDistance : the z location of the edge of the viewing area that is
    //   closest to the camera
    // type : this is how the background is configured to look.
    //   0 : fill width and height
    //   1 : fill width, adjust height accordingly
    //   2 : fill height, adjust width accordingly
    // alignment : how the picture is lined up next to the screen
    //   0 : left or top
    //   1 : middle
    //   2 : right or bottom
    public void loadMin3dBackground() {
        
        
      //  if(texId == null)
            // Log.v("JOEL","texId is null");
      //  else
            // Log.v("JOEL","texId : " + texId);
        
        // check to make sure we have a path to a texture. if not, just return.
        if(texId == null)
            return;
        
        // load up the texture
        int tint;
        
        tint = Shared.context().getResources().getIdentifier
                ((path + texId),null,null);
            
        Bitmap b = Utils.makeBitmapFromResourceId(tint);
        Shared.textureManager().addTextureId(b,texId);
        b.recycle();
        
        
        
        
        
        
        
        
        
        
        // calculate the camera to target angle
        float cameraTargetz = Math.abs(scene.camera().position.z - 
                scene.camera().target.z);
        // Log.v("JOEL","cameraTargetz : " + cameraTargetz);
        
        float cameraTargety = Math.abs(scene.camera().position.y - 
                scene.camera().target.y);
        // Log.v("JOEL","cameraTargety : " + cameraTargety);
        
        float cameraTargetTheta =
                -((float)Math.atan(cameraTargety / cameraTargetz));
        // Log.v("JOEL","cameraTargetTheta : " + cameraTargetTheta);
        
        // calculate the vertical angle on either side of the frustum.
        // this is used to calculate the bottom point. this is from a side view
        float frustumThetaVertical = -((float)Math.atan(Math.abs
                (StaticWindow.top) / StaticWindow.nearPlane));
        // Log.v("JOEL","frustumThetaVertical : " + frustumThetaVertical);
        
        // calculate the horizontal angle on either side of the frustum.
        // this is from a top view and is used to calculate the viewing width
        float frustumThetaHorizontal = 
                (float)Math.atan(Math.abs
                (StaticWindow.lt) / StaticWindow.nearPlane);
        
        // this calculates the y-value for the bottom of the background.
        // zPosition is the z-value.
        float yPosition = (float)(Math.tan(frustumThetaVertical 
                + cameraTargetTheta) * (StaticWindow.camposz - zPosition));
        // Log.v("JOEL","yPosition : " + yPosition);
        
        // now do a 2d transform to rotate the point back to the position it 
        // would be in if the camera was looking straight ahead. this allows us
        // to find the center and top points of the viewing area.
        float bottomz = (float)((Math.cos(-cameraTargetTheta) 
                * (StaticWindow.camposz - zPosition)) 
                + ((-Math.sin(-cameraTargetTheta) * yPosition)));
        // Log.v("JOEL","bottomz : " + bottomz);
        float bottomy = (float)((Math.sin(-cameraTargetTheta) 
                * (StaticWindow.camposz - zPosition)) 
                + ((Math.cos(-cameraTargetTheta) * yPosition)));
        // Log.v("JOEL","bottomy : " + bottomy);
        
        // now calculate the center point z value (y is 0)
        float rotcenterz = bottomz;
        float rotcentery = 0;
        // Log.v("JOEL","rotcenterz : " + rotcenterz);
        // Log.v("JOEL","rotcentery : " + rotcentery);
        
        // calculate the top value
        float topz = bottomz;
        float topy = -bottomy;
        // Log.v("JOEL","topz : " + topz);
        // Log.v("JOEL","topy : " + topy);
        
        // we now have the viewing height
        float viewingHeight = 2 * Math.abs(bottomy);
        // Log.v("JOEL","viewingHeight : " + viewingHeight);
        
        // calculate viewing width
        float viewingWidth = 2 * 
                (float)(Math.tan(frustumThetaHorizontal) * bottomz);
        // Log.v("JOEL","viewingWidth : " + viewingWidth);
        
        // rotate the center point back so we have the center point at the
        // rotated position
        float centerz = (float)((Math.cos(cameraTargetTheta) 
                * (rotcenterz)) 
                + ((-Math.sin(cameraTargetTheta) * rotcentery)));
        // Log.v("JOEL","centerz : " + centerz);
        float centery = (float)((Math.sin(cameraTargetTheta) 
                * (rotcenterz)) 
                + ((Math.cos(cameraTargetTheta) * rotcentery)));
        // Log.v("JOEL","centery : " + centery);
        
        // the values are based on the center being at 0,0. we need to
        // adjust these values so they are from the camera center
        centerz = StaticWindow.camposz - centerz;
        // Log.v("JOEL","centerz : " + centerz);
        centery = StaticWindow.camposy + centery;
        // Log.v("JOEL","centery : " + centery);
        
        float rectWidth = 0,rectHeight = 0;
        float rectX = 0;
        //float rectY = scene.camera().position.y - centery;
        float rectY = centery;
        float rectZ = centerz;
        
        // if we fill both width and height (may stretch picture)
        if(configure == 0) {
            rectWidth = viewingWidth;
            rectHeight = viewingHeight;
        }
        // if we fill the width and then adjust the height proportionally
        else if(configure == 1) {
            // we set the width viewing area as the width of the image.
            // now we calculate the proportional height of the viewing area
            float backHeight = viewingWidth * imgHeight / imgWidth;
            
            rectWidth = viewingWidth;
            rectHeight = backHeight;
            
            // line to top
            if(alignment == 0)
                rectY += Math.abs(viewingHeight - backHeight) / 2;
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to bottom
            else if(alignment == 2)
                rectY -= Math.abs(viewingHeight - backHeight) / 2;
        }
        // if we fill the height and then adjust the width proportionally
        else if(configure == 2) {
            // we set the height viewing area as the height of the image.
            // now we calculate the proportional width of the viewing area
            float backWidth = viewingHeight * imgWidth / imgHeight;
            
            rectWidth = backWidth;
            rectHeight = viewingHeight;
            
            // line to left
            if(alignment == 0)
                rectX = -(Math.abs(viewingWidth - backWidth) / 2);
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to right
            else if(alignment == 2)
                rectX = Math.abs(viewingWidth - backWidth) / 2;
        }
        
        Color4 color = new Color4(255,255,255,255);
        obj = new Rectangle(rectWidth,rectHeight,1,1,color);

        // put in background picture
        texVo = new TextureVo(texId);
        obj.textures().addReplace(texVo);
        texEnv = texVo.textureEnvs.get(0);
        texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

        obj.position().setAll(rectX,rectY,rectZ);
        obj.rotation().setAll
                ((float)(cameraTargetTheta * (180 / Math.PI)), 180, 0);
        //obj.rotation().setAll
        //        ((float)(cameraTargetTheta * (180 / Math.PI)), 0, 0);
        obj.scale().setAll(1, 1, 1);
        scene.addChild(obj);
        
        
        
        
        
        
        
        
        
        
        
        /*
        // calculate the horizontal angle on either side of the frustum.
        // this is from a top view
        float frustumThetaHorizontal = 
                (float)Math.atan(Math.abs
                (StaticWindow.lt) / StaticWindow.nearPlane);
        
        // calculate the angle from the camera looking straight ahead to
        // the bottom of the viewing area
        float angleToViewingBottom = cameraTargetTheta + frustumThetaVertical;
        
        // now calculate the distance from the bottom of the viewing field
        // to the camera.
        float distToViewingBottom = 
                (float)Math.abs((scene.camera().position.z - zPosition)
                / Math.cos(angleToViewingBottom));
        
        // now calculate the distance to the viewing center
        float distToViewingCenter = 
                (float)Math.cos(frustumThetaVertical) * distToViewingBottom;
        
        // now find the center point (y and z) this is where we should
        // place the background Rectangle object.
        float centery = 
                (float)Math.sin(cameraTargetTheta) * distToViewingCenter
                - StaticWindow.camposy;
        
        float centerz = 
                (float)Math.cos(cameraTargetTheta) * distToViewingCenter
                - StaticWindow.camposz;
        
        // now that we have the length to the center we can find the width
        // and height of the viewing area.
        float viewingHeight = 
                (float)Math.tan(frustumThetaVertical) * distToViewingCenter * 2;
        
        float viewingWidth = 
                (float)Math.tan(frustumThetaHorizontal) 
                * distToViewingCenter * 2;
        
        *
        // Log.v("JOEL","cam y : " + String.valueOf(cameraTargety));
        // Log.v("JOEL","cam z : " + String.valueOf(cameraTargetz));
        // Log.v("JOEL","cam theta : " + String.valueOf(cameraTargetTheta));
        // Log.v("JOEL","frustum theta vertical : " 
                + String.valueOf(frustumThetaVertical));
        // Log.v("JOEL","frustum theta horizontal : " 
                + String.valueOf(frustumThetaHorizontal));
        // Log.v("JOEL","angle to viewing area bottom : " 
                + String.valueOf(angleToViewingBottom));
        // Log.v("JOEL","distance to viewing area bottom : " 
                + String.valueOf(distToViewingBottom));
        // Log.v("JOEL","distance to viewing area center : " 
                + String.valueOf(distToViewingCenter));
        // Log.v("JOEL","center y : " 
                + String.valueOf(centery));
        // Log.v("JOEL","center z : " 
                + String.valueOf(centerz));
        // Log.v("JOEL","viewing width : " 
                + String.valueOf(viewingWidth));
        // Log.v("JOEL","viewing height : " 
                + String.valueOf(viewingHeight));
        *
        
        float rectWidth = 0,rectHeight = 0;
        float rectX = 0;
        float rectY = scene.camera().position.y - centery;
        
        // if we fill both width and height (may stretch picture)
        if(configure == 0) {
            rectWidth = viewingWidth;
            rectHeight = viewingHeight;
        }
        // if we fill the width and then adjust the height proportionally
        else if(configure == 1) {
            // we set the width viewing area as the width of the image.
            // now we calculate the proportional height of the viewing area
            float backHeight = viewingWidth * imgHeight / imgWidth;
            
            rectWidth = viewingWidth;
            rectHeight = backHeight;
            
            // line to top
            if(alignment == 0)
                rectY += Math.abs(viewingHeight - backHeight) / 2;
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to bottom
            else if(alignment == 2)
                rectY -= Math.abs(viewingHeight - backHeight) / 2;
        }
        // if we fill the height and then adjust the width proportionally
        else if(configure == 2) {
            // we set the height viewing area as the height of the image.
            // now we calculate the proportional width of the viewing area
            float backWidth = viewingHeight * imgWidth / imgHeight;
            
            rectWidth = backWidth;
            rectHeight = viewingHeight;
            
            // line to left
            if(alignment == 0)
                rectX = -(Math.abs(viewingWidth - backWidth) / 2);
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to right
            else if(alignment == 2)
                rectX = Math.abs(viewingWidth - backWidth) / 2;
        }
        
        Color4 color = new Color4(255,255,255,255);
        obj = new Rectangle(rectWidth,rectHeight,1,1,color);

        // put in background picture
        texVo = new TextureVo(texId);
        obj.textures().addReplace(texVo);
        texEnv = texVo.textureEnvs.get(0);
        texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

        obj.position().setAll(rectX,
                scene.camera().position.y - centery,
                scene.camera().position.z - centerz);
        obj.rotation().setAll
                ((float)(cameraTargetTheta * (180 / Math.PI)), 180, 0);
        //obj.rotation().setAll
        //        ((float)(cameraTargetTheta * (180 / Math.PI)), 0, 0);
        obj.scale().setAll(1, 1, 1);
        scene.addChild(obj);
        //scene.addChildAt(obj,0);
        */
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /*
        if(texId == null)
            // Log.v("JOEL","texId is null");
        else
            // Log.v("JOEL","texId : " + texId);
        
        // check to make sure we have a path to a texture. if not, just return.
        if(texId == null)
            return;
        
        // load up the texture
        int tint;
        
        tint = Shared.context().getResources().getIdentifier
                ((path + texId),null,null);
            
        Bitmap b = Utils.makeBitmapFromResourceId(tint);
        Shared.textureManager().addTextureId(b,texId);
        b.recycle();
        
        *
        // do the calculations for the frustum top,bottom,near,and far
        FrustumManaged vf = scene.camera().frustum;
	float n = vf.shortSideLength() / 2f;

	float lt, rt, btm, top;
	float aspectRatio = (float)StaticWindow.window_width 
                / (float)StaticWindow.window_height;
        
	lt  = vf.horizontalCenter() - n * aspectRatio;
        rt  = vf.horizontalCenter() + n * aspectRatio;
	btm = vf.verticalCenter() - n * 1; 
	top = vf.verticalCenter() + n * 1;

	if(aspectRatio > 1) {
            lt *= 1f / aspectRatio;
            rt *= 1f / aspectRatio;
            btm *= 1f / aspectRatio;
            top *= 1f / aspectRatio;
	}
        *
        
        // calculate the camera to target angle
        float cameraTargetz = Math.abs(scene.camera().position.z - 
                scene.camera().target.z);
        
        float cameraTargety = Math.abs(scene.camera().position.y - 
                scene.camera().target.y);
        
        float cameraTargetTheta =
                (float)Math.atan(cameraTargety / cameraTargetz);
        
        // calculate the vertical angle on either side of the frustum.
        // this is from a side view
        float frustumThetaVertical = 
                (float)Math.atan(Math.abs
                (StaticWindow.top) / StaticWindow.nearPlane);
        
        // calculate the horizontal angle on either side of the frustum.
        // this is from a top view
        float frustumThetaHorizontal = 
                (float)Math.atan(Math.abs
                (StaticWindow.lt) / StaticWindow.nearPlane);
        
        // calculate the angle from the camera looking straight ahead to
        // the bottom of the viewing area
        float angleToViewingBottom = cameraTargetTheta + frustumThetaVertical;
        
        // now calculate the distance from the bottom of the viewing field
        // to the camera.
        float distToViewingBottom = 
                (float)Math.abs((scene.camera().position.z - zPosition)
                / Math.cos(angleToViewingBottom));
        
        // now calculate the distance to the viewing center
        float distToViewingCenter = 
                (float)Math.cos(frustumThetaVertical) * distToViewingBottom;
        
        // now find the center point (y and z) this is where we should
        // place the background Rectangle object.
        float centery = 
                (float)Math.sin(cameraTargetTheta) * distToViewingCenter
                - StaticWindow.camposy;
        
        float centerz = 
                (float)Math.cos(cameraTargetTheta) * distToViewingCenter
                - StaticWindow.camposz;
        
        // now that we have the length to the center we can find the width
        // and height of the viewing area.
        float viewingHeight = 
                (float)Math.tan(frustumThetaVertical) * distToViewingCenter * 2;
        
        float viewingWidth = 
                (float)Math.tan(frustumThetaHorizontal) 
                * distToViewingCenter * 2;
        
        *
        // Log.v("JOEL","cam y : " + String.valueOf(cameraTargety));
        // Log.v("JOEL","cam z : " + String.valueOf(cameraTargetz));
        // Log.v("JOEL","cam theta : " + String.valueOf(cameraTargetTheta));
        // Log.v("JOEL","frustum theta vertical : " 
                + String.valueOf(frustumThetaVertical));
        // Log.v("JOEL","frustum theta horizontal : " 
                + String.valueOf(frustumThetaHorizontal));
        // Log.v("JOEL","angle to viewing area bottom : " 
                + String.valueOf(angleToViewingBottom));
        // Log.v("JOEL","distance to viewing area bottom : " 
                + String.valueOf(distToViewingBottom));
        // Log.v("JOEL","distance to viewing area center : " 
                + String.valueOf(distToViewingCenter));
        // Log.v("JOEL","center y : " 
                + String.valueOf(centery));
        // Log.v("JOEL","center z : " 
                + String.valueOf(centerz));
        // Log.v("JOEL","viewing width : " 
                + String.valueOf(viewingWidth));
        // Log.v("JOEL","viewing height : " 
                + String.valueOf(viewingHeight));
        *
        
        float rectWidth = 0,rectHeight = 0;
        float rectX = 0;
        float rectY = scene.camera().position.y - centery;
        
        // if we fill both width and height (may stretch picture)
        if(configure == 0) {
            rectWidth = viewingWidth;
            rectHeight = viewingHeight;
        }
        // if we fill the width and then adjust the height proportionally
        else if(configure == 1) {
            // we set the width viewing area as the width of the image.
            // now we calculate the proportional height of the viewing area
            float backHeight = viewingWidth * imgHeight / imgWidth;
            
            rectWidth = viewingWidth;
            rectHeight = backHeight;
            
            // line to top
            if(alignment == 0)
                rectY += Math.abs(viewingHeight - backHeight) / 2;
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to bottom
            else if(alignment == 2)
                rectY -= Math.abs(viewingHeight - backHeight) / 2;
        }
        // if we fill the height and then adjust the width proportionally
        else if(configure == 2) {
            // we set the height viewing area as the height of the image.
            // now we calculate the proportional width of the viewing area
            float backWidth = viewingHeight * imgWidth / imgHeight;
            
            rectWidth = backWidth;
            rectHeight = viewingHeight;
            
            // line to left
            if(alignment == 0)
                rectX = -(Math.abs(viewingWidth - backWidth) / 2);
            // if we center, do nothing. it's already there
            else if(alignment == 1) {}
            // line to right
            else if(alignment == 2)
                rectX = Math.abs(viewingWidth - backWidth) / 2;
        }
        
        Color4 color = new Color4(255,255,255,255);
        obj = new Rectangle(rectWidth,rectHeight,1,1,color);

        // put in background picture
        texVo = new TextureVo(texId);
        obj.textures().addReplace(texVo);
        texEnv = texVo.textureEnvs.get(0);
        texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

        obj.position().setAll(rectX,
                scene.camera().position.y - centery,
                scene.camera().position.z - centerz);
        obj.rotation().setAll
                ((float)(cameraTargetTheta * (180 / Math.PI)), 180, 0);
        //obj.rotation().setAll
        //        ((float)(cameraTargetTheta * (180 / Math.PI)), 0, 0);
        obj.scale().setAll(1, 1, 1);
        scene.addChild(obj);
        //scene.addChildAt(obj,0);
        */
    }
    
    
    
    /*
    public void loadMin3dBackground() {
        
        // load up the texture
        int tint;
        //String path = "upe.projectv.game.activity:drawable/p23254";
        String path = "upe.projectv.game.activity:drawable/";
        //String identifier = "p23254";
        //String identifier1 = "ex1_9";
        //String identifier = "nave_espacial2";
        //String identifier = "back_venus_galileo";
        String identifier = "back_1gif";
        
        tint = Shared.context().getResources().getIdentifier
                ((path + identifier),null,null);
            
        Bitmap b = Utils.makeBitmapFromResourceId(tint);
        Shared.textureManager().addTextureId(b,identifier);
        b.recycle();
        
        
        Color4 color = new Color4(255,255,255,255);
        obj = new Rectangle(4f,4f,1,1,color);
        //obj.lightingEnabled(false);
        
        
        // put in first picture
        texVo = new TextureVo(identifier);
        obj.textures().addReplace(texVo);
        texEnv = texVo.textureEnvs.get(0);
        texEnv.setAll(GL10.GL_TEXTURE_ENV_MODE,GL10.GL_REPLACE);
        
        
        obj.position().setAll(0,1,-10);
        obj.rotation().setAll(180,0,0);
        obj.scale().setAll(1,1,1);
        scene.addChild(obj);
    }
    */
    
    
    
    public void loadTexture() {
        
        GL10 gl = Shared.renderer().gl();
        
        textures = new int[1];
        
        //Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
        //        R.drawable.mosaic_crop);
        //Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
        //        R.drawable.ex1frame13);
        //Bitmap bitmap = BitmapFactory.decodeResource
        //        (Shared.context().getResources(),
        //        R.drawable.p23254);
        
        // load up the texture
        int tint;
        String path = "upe.projectv.game.activity:drawable/p23254";
        
        tint = Shared.context().getResources().getIdentifier
                (path,null,null);
            
        Bitmap b = Utils.makeBitmapFromResourceId(tint);
        //Shared.textureManager().addTextureId(b,path);
        //b.recycle();
        //GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0,b,0);
        //b.recycle();
        
        gl.glGenTextures(1,textures,0);
        gl.glBindTexture(GL10.GL_TEXTURE_2D,textures[0]);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MIN_FILTER,
                GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MAG_FILTER,
                GL10.GL_LINEAR);
        
        // NEW FOR TRANSPARENT TEXTURES
        gl.glTexEnvf(GL10.GL_TEXTURE_ENV,GL10.GL_TEXTURE_ENV_MODE,
                GL10.GL_REPLACE);
        
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0,b,0);
        b.recycle();
    }
    
    public void drawOrthoBackground3() {
        
        GL10 gl = Shared.renderer().gl();
        
        
        
        FrustumManaged vf = scene.camera().frustum;
        float n = vf.shortSideLength() / 2f;

        float lt, rt, btm, top;

        lt = vf.horizontalCenter() - n * surfaceAspectRatio;
        rt = vf.horizontalCenter() + n * surfaceAspectRatio;
        btm = vf.verticalCenter() - n * 1;
        top = vf.verticalCenter() + n * 1;

        if (surfaceAspectRatio > 1) {
            lt *= 1f / surfaceAspectRatio;
            rt *= 1f / surfaceAspectRatio;
            btm *= 1f / surfaceAspectRatio;
            top *= 1f / surfaceAspectRatio;
        }
        
        
        
        // get into ortho mode
        gl.glDisable(GL10.GL_DEPTH_TEST);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        //gl.glOrthof(lt,rt,btm,top,-1,1);
        gl.glOrthof(lt,rt,btm,top,vf.zNear(),vf.zFar());
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
        
        // draw the ortho stuff
        // draw the OpenGL stuff here
        FloatBuffer vbuf;
        ShortBuffer sbuf;
        
        // FOR THE TEXTURE MAPPING
        FloatBuffer fbuf;

        float[] texturePoints = new float[]{
            0f, 1f,
            0f, 0f,
            1f, 1f,
            1f, 0f
        };
        /*
        float[] verticesSquare = new float[]{
            -1, 1f, 0f,
            -1f, -1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        */
        float[] verticesSquare = new float[]{
            -1f, -1f, 0f,
            -1, 1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        
        //short[] indicesSquare = new short[]{0, 1, 2, 0, 2, 3};
        int numVerticesSquare = 6;


        /*
        float[] verticesTriangle = new float[]{
            -1f, 1f, 0f,
            -1, -1f, 0f,
            1f, -1f, 0f
        };
        short[] indicesTriangle = new short[]{0, 1, 2};
        int numVerticesTriangle = 3;

        float zpos = -1.0f;
        */

        // MOVE TO CONSTRUCTOR
        ByteBuffer vbb = ByteBuffer.allocateDirect(numVerticesSquare * 3 * 4);
        vbb.order(ByteOrder.nativeOrder());
        vbuf = vbb.asFloatBuffer();
        vbuf.put(verticesSquare);
        vbuf.position(0);

        ByteBuffer tbb = ByteBuffer.allocateDirect(texturePoints.length * 4);
        //ByteBuffer ibb = ByteBuffer.allocateDirect(18);
        tbb.order(ByteOrder.nativeOrder());
        fbuf = tbb.asFloatBuffer();
        fbuf.put(texturePoints);
        fbuf.position(0);
        // END MOVE TO CONSTRUCTOR




        gl.glPushMatrix();
        //gl.glLoadIdentity();
        gl.glTranslatef(1f,0f,-10f);

        
        gl.glBindTexture(GL10.GL_TEXTURE_2D,textures[0]);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        
        //gl.glEnable(GL10.GL_BLEND);
        //gl.glBlendFunc(GL10.GL_SRC_ALPHA,GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL10.GL_TEXTURE_2D);

        gl.glFrontFace(GL10.GL_CW);
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glCullFace(GL10.GL_BACK);

        gl.glColor4f(1.0f,1.0f,1.0f,1.0f);
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vbuf);
        gl.glTexCoordPointer(2,GL10.GL_FLOAT,0,fbuf);
        //gl.glDrawElements(GL10.GL_TRIANGLES,numVerticesSquare,
        //        GL10.GL_UNSIGNED_SHORT,sbuf);
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,verticesSquare.length / 3);

        gl.glDisable(GL10.GL_CULL_FACE);
        
        //gl.glDisable(GL10.GL_BLEND);
        gl.glDisable(GL10.GL_TEXTURE_2D);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glPopMatrix();
        
        
        
        // switch back to perspective
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glPopMatrix();
        
        
    }
    
    public void drawOrthoBackground2() {
        
        GL10 gl = Shared.renderer().gl();
        
        
        
        FrustumManaged vf = scene.camera().frustum;
        float n = vf.shortSideLength() / 2f;

        float lt, rt, btm, top;

        lt = vf.horizontalCenter() - n * surfaceAspectRatio;
        rt = vf.horizontalCenter() + n * surfaceAspectRatio;
        btm = vf.verticalCenter() - n * 1;
        top = vf.verticalCenter() + n * 1;

        if (surfaceAspectRatio > 1) {
            lt *= 1f / surfaceAspectRatio;
            rt *= 1f / surfaceAspectRatio;
            btm *= 1f / surfaceAspectRatio;
            top *= 1f / surfaceAspectRatio;
        }

        //gl.glMatrixMode(GL10.GL_PROJECTION);
        //gl.glLoadIdentity();
        //gl.glFrustumf(lt, rt, btm, top, vf.zNear(), vf.zFar());
        
        
        
        gl.glDisable(GL10.GL_CULL_FACE);
        gl.glDisable(GL10.GL_DEPTH_TEST);
        
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        
        
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        //gl.glFrustumf(lt, rt, btm, top, vf.zNear(), vf.zFar());
        gl.glOrthof(lt, rt, btm, top, vf.zNear(), vf.zFar());
        
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        
        
        
        
        // draw the OpenGL stuff here
        FloatBuffer vbuf;
        ShortBuffer sbuf;
        
        // FOR THE TEXTURE MAPPING
        FloatBuffer fbuf;

        float[] texturePoints = new float[]{
            0f, 1f,
            0f, 0f,
            1f, 1f,
            1f, 0f
        };
        /*
        float[] verticesSquare = new float[]{
            -1, 1f, 0f,
            -1f, -1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        */
        float[] verticesSquare = new float[]{
            -1f, -1f, 0f,
            -1, 1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        
        //short[] indicesSquare = new short[]{0, 1, 2, 0, 2, 3};
        int numVerticesSquare = 6;


        /*
        float[] verticesTriangle = new float[]{
            -1f, 1f, 0f,
            -1, -1f, 0f,
            1f, -1f, 0f
        };
        short[] indicesTriangle = new short[]{0, 1, 2};
        int numVerticesTriangle = 3;

        float zpos = -1.0f;
        */

        // MOVE TO CONSTRUCTOR
        ByteBuffer vbb = ByteBuffer.allocateDirect(numVerticesSquare * 3 * 4);
        vbb.order(ByteOrder.nativeOrder());
        vbuf = vbb.asFloatBuffer();
        vbuf.put(verticesSquare);
        vbuf.position(0);

        ByteBuffer tbb = ByteBuffer.allocateDirect(texturePoints.length * 4);
        //ByteBuffer ibb = ByteBuffer.allocateDirect(18);
        tbb.order(ByteOrder.nativeOrder());
        fbuf = tbb.asFloatBuffer();
        fbuf.put(texturePoints);
        fbuf.position(0);
        // END MOVE TO CONSTRUCTOR




        gl.glPushMatrix();
        //gl.glLoadIdentity();
        gl.glTranslatef(1f,0f,-10f);

        
        gl.glBindTexture(GL10.GL_TEXTURE_2D,textures[0]);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        
        //gl.glEnable(GL10.GL_BLEND);
        //gl.glBlendFunc(GL10.GL_SRC_ALPHA,GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL10.GL_TEXTURE_2D);

        gl.glFrontFace(GL10.GL_CW);
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glCullFace(GL10.GL_BACK);

        gl.glColor4f(1.0f,1.0f,1.0f,1.0f);
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vbuf);
        gl.glTexCoordPointer(2,GL10.GL_FLOAT,0,fbuf);
        //gl.glDrawElements(GL10.GL_TRIANGLES,numVerticesSquare,
        //        GL10.GL_UNSIGNED_SHORT,sbuf);
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,verticesSquare.length / 3);

        gl.glDisable(GL10.GL_CULL_FACE);
        
        //gl.glDisable(GL10.GL_BLEND);
        gl.glDisable(GL10.GL_TEXTURE_2D);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glPopMatrix();
        
        
        
        
        // change back to perspective
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        
        
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glFrustumf(lt,rt, btm,top, vf.zNear(), vf.zFar());
        
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        
    }
    
    public void drawOrthoBackground() {
        
        surfaceAspectRatio = (float)StaticWindow.window_width 
                / (float)StaticWindow.window_height;
        
        GL10 gl = Shared.renderer().gl();
        
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
        // do the calculations for the ortho (similar to updateViewFrustum in
        // Renderer).
        FrustumManaged vf = scene.camera().frustum;
        float n = vf.shortSideLength() / 2f;

	float lt, rt, btm, top;
		
	lt  = vf.horizontalCenter() - n * surfaceAspectRatio;
	rt  = vf.horizontalCenter() + n * surfaceAspectRatio;
	btm = vf.verticalCenter() - n * 1; 
	top = vf.verticalCenter() + n * 1;

	if (surfaceAspectRatio > 1) {
            lt *= 1f/surfaceAspectRatio;
            rt *= 1f/surfaceAspectRatio;
            btm *= 1f/surfaceAspectRatio;
            top *= 1f/surfaceAspectRatio;
	}
		        
        gl.glOrthof(lt,rt,btm,top,vf.zNear(),vf.zFar());
        
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        
        
        
        
        
        // draw the OpenGL stuff here
        FloatBuffer vbuf;
        ShortBuffer sbuf;
        
        // FOR THE TEXTURE MAPPING
        FloatBuffer fbuf;

        float[] texturePoints = new float[]{
            0f, 1f,
            0f, 0f,
            1f, 1f,
            1f, 0f
        };
        /*
        float[] verticesSquare = new float[]{
            -1, 1f, 0f,
            -1f, -1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        */
        float[] verticesSquare = new float[]{
            -1f, -1f, 0f,
            -1, 1f, 0f,
            1f, -1f, 0f,
            1f, 1f, 0f
        };
        
        //short[] indicesSquare = new short[]{0, 1, 2, 0, 2, 3};
        int numVerticesSquare = 6;


        /*
        float[] verticesTriangle = new float[]{
            -1f, 1f, 0f,
            -1, -1f, 0f,
            1f, -1f, 0f
        };
        short[] indicesTriangle = new short[]{0, 1, 2};
        int numVerticesTriangle = 3;

        float zpos = -1.0f;
        */

        // MOVE TO CONSTRUCTOR
        ByteBuffer vbb = ByteBuffer.allocateDirect(numVerticesSquare * 3 * 4);
        vbb.order(ByteOrder.nativeOrder());
        vbuf = vbb.asFloatBuffer();
        vbuf.put(verticesSquare);
        vbuf.position(0);

        ByteBuffer tbb = ByteBuffer.allocateDirect(texturePoints.length * 4);
        //ByteBuffer ibb = ByteBuffer.allocateDirect(18);
        tbb.order(ByteOrder.nativeOrder());
        fbuf = tbb.asFloatBuffer();
        fbuf.put(texturePoints);
        fbuf.position(0);
        // END MOVE TO CONSTRUCTOR




        gl.glPushMatrix();
        //gl.glLoadIdentity();
        gl.glTranslatef(1f,0f,-10f);

        
        gl.glBindTexture(GL10.GL_TEXTURE_2D,textures[0]);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        
        //gl.glEnable(GL10.GL_BLEND);
        //gl.glBlendFunc(GL10.GL_SRC_ALPHA,GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL10.GL_TEXTURE_2D);

        gl.glFrontFace(GL10.GL_CW);
        gl.glEnable(GL10.GL_CULL_FACE);
        gl.glCullFace(GL10.GL_BACK);

        gl.glColor4f(1.0f,1.0f,1.0f,1.0f);
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vbuf);
        gl.glTexCoordPointer(2,GL10.GL_FLOAT,0,fbuf);
        //gl.glDrawElements(GL10.GL_TRIANGLES,numVerticesSquare,
        //        GL10.GL_UNSIGNED_SHORT,sbuf);
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,verticesSquare.length / 3);

        gl.glDisable(GL10.GL_CULL_FACE);
        
        //gl.glDisable(GL10.GL_BLEND);
        gl.glDisable(GL10.GL_TEXTURE_2D);

        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        gl.glPopMatrix();
        
        
        
        
        
        // GL ORTHO HERE 
        gl.glPopMatrix();
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glPopMatrix();
        
        
        //gl.glLoadIdentity();
        
        
        
        
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        
    }
    
}
