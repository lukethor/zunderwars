 package upe.projectv.game.activity;


	
	import java.io.File;
import java.io.IOException;
	import java.io.StringReader;
	import java.io.UnsupportedEncodingException;
	import java.net.MalformedURLException;

	import javax.xml.parsers.DocumentBuilder;
	import javax.xml.parsers.DocumentBuilderFactory;
	import javax.xml.parsers.ParserConfigurationException;

	import org.apache.http.HttpEntity;
	import org.apache.http.HttpResponse;
	import org.apache.http.client.methods.HttpPost;
	import org.apache.http.impl.client.DefaultHttpClient;
	import org.apache.http.util.EntityUtils;
	import org.w3c.dom.Document;
	import org.w3c.dom.Element;
	import org.w3c.dom.Node;
	import org.w3c.dom.NodeList;
	import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.os.Environment;
import android.util.Log;








	public class XMLfunctions {
		 public static boolean scoreisgreater = false;
		 public static	int lowest = -1;
		 public static int index = 0;
		 public final static Document XMLfromString(String xml){
		     Document doc = null;
			
			 
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        try {
	        	
				DocumentBuilder db = dbf.newDocumentBuilder();
				
				InputSource is = new InputSource();
		        is.setCharacterStream(new StringReader(xml));
		        doc = db.parse(is); 
		        
			} catch (ParserConfigurationException e) {
				System.out.println("XML parse error: " + e.getMessage());
				return null;
			} catch (SAXException e) {
				System.out.println("Wrong XML file structure: " + e.getMessage());
	            return null;
			} catch (IOException e) {
				System.out.println("I/O exeption: " + e.getMessage());
				return null;
			}
			       
	        return doc;
	        
		}
		
		/** Returns element value
		  * @param elem element (it is XML tag)
		  * @return Element value otherwise empty String
		  */
		 public final static String getElementValue( Node elem ) {
		     Node kid;
		     if( elem != null){
		         if (elem.hasChildNodes()){
		             for( kid = elem.getFirstChild(); kid != null; kid = kid.getNextSibling() ){
		                 if( kid.getNodeType() == Node.TEXT_NODE  ){
		                     return kid.getNodeValue();
		                 }
		             }
		         }
		     }
		     return "";
		 }
			 
		public static String getWebXML(String path){	 
				String line = null;

				try {
					
					DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(path);

					HttpResponse httpResponse = httpClient.execute(httpPost);
					HttpEntity httpEntity = httpResponse.getEntity();
					line = EntityUtils.toString(httpEntity);
					
				} catch (UnsupportedEncodingException e) {
					line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
				} catch (MalformedURLException e) {
					line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
				} catch (IOException e) {
					line = "<results status=\"error\"><msg>Can't connect to server</msg></results>";
				}

				return line;

		}
		 
		public static String getInternalXML() {
			String XML = "";
			try {
				
				File f = new File(FrogListScore.xmlPath , FrogListScore.xmlFile);
				
				if (f.exists()) {		    
				    XML = XMLFileCreator.read();			
				}else{
					XML = XMLFileCreator.create();				
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(FrogListScore.LOGTAG, e.getMessage());			
			}
			
			return XML;
		}	
		
		public static boolean saveScore(String name, int score){
			Document doc = null;	
			NodeList nodes = null;
			
			String xml = "";
			try {
				xml = XMLFileCreator.read();

				doc = XMLfromString(xml);	
				nodes = doc.getElementsByTagName("result");

			
			

			
	
		   
			for (int i = 0; i < nodes.getLength(); i++) {										
				Element e = (Element)nodes.item(i);			
				int s = Integer.parseInt(XMLfunctions.getValue(e, "score"));	
				if ( s < score ){
				if((lowest == -1) || (s < lowest)){
					lowest = s;
					index = i;
				}
				scoreisgreater = true;
				}
			}			
	        
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if ( scoreisgreater )
         			return XMLFileCreator.save(doc, name, score, index);
			else
				return true;
		}
		
		public static int numResults(Document doc){		
			Node results = doc.getDocumentElement();
			int res = -1;
			
			try{
				res = Integer.valueOf(results.getAttributes().getNamedItem("count").getNodeValue());
			}catch(Exception e ){
				res = -1;
			}
			
			return res;
		}
		
		public static String getValue(Element item, String str) {		
			NodeList n = item.getElementsByTagName(str);		
			return XMLfunctions.getElementValue(n.item(0));
		}	
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	