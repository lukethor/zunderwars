package upe.projectv.game.managers.gamemgmt;

import android.util.Log;
import min3d.core.Scene;
import min3d.vos.Number3d;
import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.managers.objectmgmt.HeroManager;
import upe.projectv.game.managers.objectmgmt.HeroShotManager;
import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.managers.objectmgmt.VillainShotManager;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;

public class LoadCharacters implements Runnable {
    
    private boolean done;
    
    private Scene scene;
    private HeroManager hMgr;
    private HeroShotManager hsMgr;
    private ExplosionManager eMgr;
    private VillainManager vMgr;
    private VillainShotManager vsMgr;
    private BackgroundManager backMgr;
    
    public LoadCharacters(GameManager gm) {
        
        this.scene = gm.scene;
        this.hMgr = gm.hMgr;
        this.hsMgr = gm.hsMgr;
        this.eMgr = gm.eMgr;
        this.vMgr = gm.vMgr;
        this.vsMgr = gm.vsMgr;
        this.backMgr = gm.backMgr;
        done = false;
    }
    
    @Override
    public void run() {
        
        try {
            
            // load up the sounds. not quite characters, but no real
            // need to create a whole separate thread just for the audio.
            //SoundHandler.initSoundHandler();
            
            vMgr.setObject3dContainers();
            vsMgr.setObject3dContainers();
            
            backMgr.loadMin3dBackground();
            
            hsMgr.setObject3dContainers();
            hMgr.setObject3dContainers();
            
            // load the explosions last
            eMgr.setObject3dContainers();
            
            done = true;    
        }
        catch(Exception e) {
            // Log.v("JOEL","Exception in LoadCharacters thread");
        }
    }
    
    public boolean isDone() {
        
        return done;
    }
    
    public void killThread() {
        
        eMgr = null;
        backMgr = null;
        hsMgr = null;
        hMgr = null;
        vMgr = null;
        vsMgr = null;
        // Log.v("JOEL","in kill function");
    }
}
