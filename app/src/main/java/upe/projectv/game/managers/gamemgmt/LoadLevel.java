package upe.projectv.game.managers.gamemgmt;

import upe.projectv.game.managers.objectmgmt.VillainManager;
import upe.projectv.game.managers.objectmgmt.HeroManager;
import upe.projectv.game.managers.objectmgmt.HeroShotManager;
import upe.projectv.game.managers.objectmgmt.ExplosionManager;
import upe.projectv.game.managers.objectmgmt.BackgroundManager;
import upe.projectv.game.levels.iLevel;
import min3d.vos.Number3d;
import android.util.Log;

public class LoadLevel implements Runnable {
    
    private boolean done;
    private HeroManager hMgr;
    private HeroShotManager hsMgr;
    private ExplosionManager eMgr;
    private VillainManager vMgr;
    private BackgroundManager backMgr;
    private iLevel level;
    
    public LoadLevel(GameManager gm,iLevel level) {
        
        this.hMgr = gm.hMgr;
        this.hsMgr = gm.hsMgr;
        this.eMgr = gm.eMgr;
        this.vMgr = gm.vMgr;
        this.backMgr = gm.backMgr;
        this.level = level;
        
        done = false;
    }
    
    @Override
    public void run() {
        
        // set the background parameters
        backMgr.setBackgroundParameters(level.getBackgroundPath(),
                level.getBackgroundWidth(),level.getBackgroundHeight(),
                level.getBackgroundzPosition(),
                level.getBackgroundConfigure(),level.getBackgroundAlignment());
        // load up the background
        backMgr.loadMin3dBackground();
        
        // load in the villains
        float[] villains;
        Number3d tempNumberPosition;
        if((villains = level.getVillain0Info()) != null) {
            
            for(int i = 0; i < villains.length / 4; i++) {
                
                tempNumberPosition = new Number3d(villains[i * 4],
                        villains[i * 4 + 1],villains[i * 4 + 2]);
                vMgr.loadVillainIntoLevel
                        (tempNumberPosition,(int)villains[i * 4 + 3],0);
            }
        }
        
        if((villains = level.getVillain1Info()) != null) {
            
            for(int i = 0; i < villains.length / 4; i++) {
                
                tempNumberPosition = new Number3d(villains[i * 4],
                        villains[i * 4 + 1],villains[i * 4 + 2]);
                vMgr.loadVillainIntoLevel
                        (tempNumberPosition,(int)villains[i * 4 + 3],1);
            }
        }
        
        // load up the hero
        hMgr.loadShipIntoLevel(new Number3d(0,0,-7.2f));
        
        done = true;
    }
    
    public boolean isDone() {
        return done;
    }
}
