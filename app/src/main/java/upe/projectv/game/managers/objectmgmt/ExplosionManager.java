package upe.projectv.game.managers.objectmgmt;

/**
 *
 * @author jbomb
 */

import android.util.Log;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

import java.util.ArrayList;

import min3d.Shared;
import upe.projectv.game.gameobjects.iShip;
import upe.projectv.game.gameobjects.iExplosion;
import upe.projectv.game.gameobjects.Explosion0;
import min3d.core.Scene;
import min3d.core.Object3dContainer;
import android.opengl.GLUtils;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import min3d.Utils;
import android.opengl.GLUtils;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import upe.projectv.game.statics.StaticWindow;

public class ExplosionManager {
    
    private Scene scene;
    private ArrayList<iExplosion> activeExplosions;
    private Iterator<iExplosion> eIterator;
    private iExplosion eTemp;
    
    public ExplosionManager(Scene scene) {
        
        this.scene = scene;
        
        activeExplosions = new ArrayList<iExplosion>();
        /*
        inactiveExplosion0 = new LinkedList<iExplosion>();
        
        // create inactive explosions
        for(int i = 0; i < 10; i++) {
            
            eTemp = new Explosion0();
            inactiveExplosion0.addLast(eTemp);
        }
        */
    }
    
    public void explosionFinished(iExplosion explosion) {
        
        // remove the explosion from the active queue
        activeExplosions.remove(explosion);
        
        // remove the explosion from the scene
        scene.removeChild(explosion.getObject3dContainer());
        
        // clear the object3dcontainer for the garbage collector
        explosion.getObject3dContainer().clear();
    }
    
    public void clearActiveQueues() {
        
        
        eIterator = activeExplosions.iterator();
        
        while(eIterator.hasNext()) {
            
            eTemp = eIterator.next();
            
            eIterator.remove();
            
            scene.removeChild(eTemp.getObject3dContainer());
            
            eTemp.getObject3dContainer().clear();
        }
    }
    
    public void setObject3dContainers() {
        
        // load the explosion textures
        loadExplosionTextures();
        
        // iterate through the explosions, load them
        // and add them to the scene
        eIterator = activeExplosions.iterator();

        while (eIterator.hasNext()) {
            eTemp = eIterator.next();
            
            //Object3dContainer o =
            //        new Rectangle(6f,8.45f,1,1,new Color4(255,255,255,255));
            Object3dContainer o =
                    new Rectangle(6f,12f,1,1,new Color4(255,255,255,255));
            
            eTemp.setObject3dContainer(o);
            scene.addChildAt(eTemp.getObject3dContainer(),
                    scene.numChildren());
        }
    }
    
    // this sets up the explosion to be added into the scene
    // and links it with the ship it will be "exploding"
    public iExplosion getExplosion(iShip ship) {
        
        // create a new explosion
        iExplosion explosion = new Explosion0();
        
        if(ship.getType() == 0)
            explosion.setTranslation(ship.getObject3dContainer().position().x,
                    ship.getObject3dContainer().position().y,
                    ship.getObject3dContainer().position().z
                    + ship.getBoundingBox()[3]);
        else if(ship.getType() == 1)
            explosion.setTranslation(
                    ship.getObject3dContainer().position().x,
                    ship.getObject3dContainer().position().y + 2.213207675f,
                    ship.getObject3dContainer().position().z
                    + ship.getBoundingBox()[3]);
        
        Object3dContainer o =
                    new Rectangle(6f,8.45f,1,1,new Color4(255,255,255,255));
        
        o.rotation().setAll(0,0,180);
        
        explosion.setObject3dContainer(o);
        
        // iterate through the active explosions. if the new explosion's z
        // value is closer to the camera (>) add 1 to the variable and keep
        // going. once it is < a value, place it behind that value. then add
        // it to the scene at (scene.numChildren - numActiveExplosions) +
        // the variable of how many values it is <. this is for transparency.
        // the explosions closest to the camera must be drawn last or they 
        // won't be transparent (OpenGL does blending, not transparency).
        int position = 0;
        
        for(int i = 0; i < activeExplosions.size(); i++) {
            
            eTemp = activeExplosions.get(i);
            
            if(explosion.getObject3dContainer().position().z
                    < eTemp.getObject3dContainer().position().z)
                break;
            else
                position += 1;
        }
        
        scene.addChildAt(explosion.getObject3dContainer(),
                scene.numChildren() - activeExplosions.size() + position);
        activeExplosions.add(position,explosion);
        
        return explosion;
    }
    
    // this sets up the explosion to be added into the scene
    // and links it with the ship it will be "exploding"
    public iExplosion getHeroExplosion(iShip ship) {
        
        // create a new explosion
        iExplosion explosion = new Explosion0();
        
        
        
        
        
        
        
        
        
        
        
        // here we need to figure out where the hero is and move the
        // explosion accordingly up and to the left or right
        float tempPercentage = 
                Math.abs(ship.getObject3dContainer().position().x) 
                / StaticWindow.herox;
        //float tempx = tempPercentage * 0.5f;
        //float tempy = tempPercentage * 1.0f;
        float tempx = tempPercentage * 0.8f;
        float tempy = tempPercentage * 1.0f + .4f;
        
        // if the hero is to the left we will need to move up and to the right
        if(ship.getObject3dContainer().position().x < 0) {}
        // if the hero is centered or to the right, we up and to the left
        else {
            
            tempx = -tempx;
        }
        
        explosion.setTranslation(
                ship.getObject3dContainer().position().x + tempx,
                ship.getObject3dContainer().position().y + tempy,
                ship.getObject3dContainer().position().z
                + ship.getBoundingBox()[3]);
        
        /*
        explosion.setTranslation(
                ship.getObject3dContainer().position().x + 0.5f,
                ship.getObject3dContainer().position().y + 1.0f,
                ship.getObject3dContainer().position().z
                + ship.getBoundingBox()[3]);
        */
        
        
        
        
        
        
        
        
        
        Object3dContainer o =
                    new Rectangle(6f,8.45f,1,1,new Color4(255,255,255,255));
        
        explosion.setObject3dContainer(o);
        
        // iterate through the active explosions. if the new explosion's z
        // value is closer to the camera (>) add 1 to the variable and keep
        // going. once it is < a value, place it behind that value. then add
        // it to the scene at (scene.numChildren - numActiveExplosions) +
        // the variable of how many values it is <. this is for transparency.
        // the explosions closest to the camera must be drawn last or they 
        // won't be transparent (OpenGL does blending, not transparency).
        int position = 0;
        
        for(int i = 0; i < activeExplosions.size(); i++) {
            
            eTemp = activeExplosions.get(i);
            
            if(explosion.getObject3dContainer().position().z
                    < eTemp.getObject3dContainer().position().z)
                break;
            else
                position += 1;
        }
        
        scene.addChildAt(explosion.getObject3dContainer(),
                scene.numChildren() - activeExplosions.size() + position);
        activeExplosions.add(position,explosion);
        
        return explosion;
    }
    
    // this loads up the textures for the explosions
    public void loadExplosionTextures() {
        
        int tint;
        String path = "upe.projectv.game.activity:drawable/";
        String tid;
        for(int i = 0; i < 16; i++) {
            tid =  "ex1_" + i;
            tint = Shared.context().getResources().getIdentifier
                    ((path + tid),null,null);
            
            Bitmap b = Utils.makeBitmapFromResourceId(tint);
            Shared.textureManager().addTextureId(b,tid);
            b.recycle();
        }
    }
    
    // this function goes through the active shots and saves their current
    // status
    public void saveStatus() {
        
        // iterate through the shots and save them
        eIterator = activeExplosions.iterator();

        while (eIterator.hasNext()) {
            eTemp = eIterator.next();
            eTemp.saveStatus();
        }
    }
    
    public ArrayList<iExplosion> getActiveExplosions() {
        return activeExplosions;
    }
    
}
