/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package upe.projectv.game.statics;

/**
 *
 * @author jbomb
 */

import java.util.concurrent.Semaphore;

// to make the GameCycle and Renderer thread-safe, we need a mutex. this class
// handles that. it is static so we don't have to make any significant 
// modifications to Renderer and RendererActivity
public class GLThreadSync {
    
    private static Semaphore glSemaphore =
            new Semaphore(1);
    
    public static boolean getGLSemaphore() {
        
        try {
            glSemaphore.acquire();
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }
    
    public static void releaseGLSemaphore() {
        glSemaphore.release();
    }
}
