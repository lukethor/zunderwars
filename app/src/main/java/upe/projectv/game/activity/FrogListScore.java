package upe.projectv.game.activity;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



import android.app.ListActivity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.Collections;




public class FrogListScore extends ListActivity {
	public static Context CONTEXT;
	public static String LOGTAG = "zunderwars";
	
	public static String xmlPath = "/sdcard/zunderwars";
	public static String xmlFile = "score.xml";
	
	public ArrayList<HashMap<String, String>> mylist;
	public ListAdapter adapter;
	
	Global global;
        
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listplaceholder);
        
        CONTEXT = this.getBaseContext();
                
        mylist = new ArrayList<HashMap<String, String>>();	
        
        adapter = new SimpleAdapter(this, mylist , R.layout.main, 
                new String[] { "name", "Score" }, 
                new int[] { R.id.item_title, R.id.item_subtitle });
        
        loadData();	
		/*        
        final ListView lv = getListView();
        lv.setTextFilterEnabled(true);	
        lv.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {        		
        		@SuppressWarnings("unchecked")
				HashMap<String, String> o = (HashMap<String, String>) lv.getItemAtPosition(position);	        		
        		Toast.makeText(FrogListScore.this, "ID '" + o.get("id") + "' was clicked.", Toast.LENGTH_LONG).show(); 

			}
		});
		
		*/
    }
    
    public void loadData(){
    	mylist.clear();
    	
    	//Get the contents of the xml or creates a standard file
        String xml = XMLfunctions.getInternalXML();
        Document doc = XMLfunctions.XMLfromString(xml);
                
        int numResults = XMLfunctions.numResults(doc);
        
        if((numResults <= 0)){
        	Toast.makeText(FrogListScore.this, "No Scores to Display", Toast.LENGTH_LONG).show();  
        	finish();
        }               
        
		NodeList nodes = doc.getElementsByTagName("result");
	    
		
		int[] intArray = new int[] {0, 0, 0};
		Integer s;
		int j=0;
		int i;
		for (j = 0; j < nodes.getLength(); j++) {
			
			Element e = (Element)nodes.item(j);			
			s = Integer.parseInt(XMLfunctions.getValue(e, "score"));	
			intArray[j] = s;
			
		}
		Arrays.sort(intArray);
		//Arrays.sort(intArray, Collections.reverseOrder());

	 //    for ( i = 0; i < intArray.length; i++){
	 //           System.out.println(intArray[i]);
	      
	 //     }
		
		for ( i = 0; i < nodes.getLength() ; i++) {							
			HashMap<String, String> map = new HashMap<String, String>();	
			

			for ( j =0 ; j < nodes.getLength(); j++ )
			{
			
				Element e = (Element)nodes.item(j);
				s = Integer.parseInt(XMLfunctions.getValue(e, "score"));	
			if ( s == intArray[nodes.getLength() - (i+1)] ){
			//	System.out.println("Iam here" + s );
			map.put("id", XMLfunctions.getValue(e, "id"));
        	map.put("name", "Player:" + XMLfunctions.getValue(e, "name"));
        	map.put("Score", "Score: " + XMLfunctions.getValue(e, "score"));
        //	map.put("intscore", XMLfunctions.getValue(e, "score"));
        	mylist.add(map);
        	break;
			}
		}	
			
		}
		
		setListAdapter(adapter);
    }
    
    /* MENU FUNCTION */
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	boolean success = false;
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.addscore:
            success = XMLfunctions.saveScore("Test score", 150);
            if(success)
            	loadData();
            return true;
        case R.id.resetscore:
        	success = XMLFileCreator.delete();
        	if(success)
            	loadData();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
   

    
	@Override
	protected void onDestroy() {
	//	Global.mp.stop();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
	
	//	Global.startMusic(this);
		super.onResume();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	
	
	
	
    
}
