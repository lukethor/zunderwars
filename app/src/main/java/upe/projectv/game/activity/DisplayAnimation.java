package upe.projectv.game.activity;





import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class DisplayAnimation extends Activity {
    /** Called when the activity is first created. */
	
	private AnimationDrawable animation;
	public static short myTimer;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation);
        
        Global.mp = MediaPlayer.create(this, R.raw.nullsleep);
        Global.mp.setLooping(true);
        
        final ImageView img = (ImageView) findViewById(R.id.animationview);        
        img.setBackgroundResource(R.drawable.zilla3danim);
        animation = (AnimationDrawable) img.getBackground();
        
        
        
        getSettings();   
	    Global.startMusic(this);	       
	    Toast.makeText(this, "Welcome back, " + Global.playername, Toast.LENGTH_SHORT).show();
       
	    img.post(new Starter());     
        
        
    
        Thread logoTimer = new Thread(){        	
	        public void run(){
	        try{
	        	Thread.sleep(4000L);
	        	img.post(new Starter());
	        } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         finally {
           
                startActivity( new Intent(DisplayAnimation.this,FrogzillaSpalsh.class));
                finish();
            
            }
	        
	       }  
       
        };
	      
        logoTimer.start();
	
    }
    
    

	class Starter implements Runnable {
        public void run() {
            animation.start();      
        }     
    }
        
    @Override
	protected void onDestroy() {    	    	
		// TODO Auto-generated method stub
		super.onDestroy();
		Global.mp.stop();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}


	
	private void getSettings() {
		SharedPreferences settings = getSharedPreferences(FrogzillaSpalsh.PREFS_NAME, 0);
		
		Global.settings[0] = settings.getBoolean("sound", true);
		Global.settings[1] = settings.getBoolean("music", true);
		Global.settings[2] = settings.getBoolean("vibrate", true);
		Global.settings[3] = settings.getBoolean("graphics", true);	
		
		Global.playername = settings.getString("player", "Zunder3D");	
	}
	
	
	
	

}
