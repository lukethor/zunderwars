package upe.projectv.game.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;
 
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
import android.widget.TextView;
import android.widget.Toast;


 
public class XMLFileCreator extends Activity {
	public static String create() throws IOException{
		
		//String FILENAME = "score.xml";
		
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
			Toaster("We can read and write scores");
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
			Toaster("We can read only scores");
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			// to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
			Toaster("Something wrong with writing scores");
		}
		
		//Storage NOK ? Then display a message and exit :-)
		if(!mExternalStorageAvailable || !mExternalStorageWriteable){
			Toaster("The sdcast is not readable or is not available");
			return "error";
		}
		
		// create a File object for the parent directory		
		File ZillaDirectory = new File(FrogListScore.xmlPath);
		
		// have the object build the directory structure, if needed.		
		@SuppressWarnings("unused")
		boolean b = ZillaDirectory.mkdirs();
		    
		// create a File object for the output file		
		File outputFile = new File(ZillaDirectory, FrogListScore.xmlFile);
		
		//create a new file called "score.xml" in the SD card
		//we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		//File newxmlfile = new File(Environment.getExternalStorageDirectory()+"/sdcard/zilla/score.xml");
		 
		try{
			fileos = new FileOutputStream(outputFile,false);		
		}catch(IOException e){
			Toaster("Cannot open file '"+ outputFile.getAbsolutePath().toString() +"'");
			return "error";
		}
		
		//we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		
		try {
			//we set the FileOutputStream as output for the serializer, using UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			//Write <?xml declaration with encoding (if encoding not null) and standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			//set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			//start a tag called "root"
			serializer.startTag(null, "results");
			serializer.attribute("", "count", "6");
			serializer.startTag(null, "result");
			//i indent code just to have a view similar to xml-tree
			serializer.startTag(null, "id");
			serializer.text("1");
			serializer.endTag(null, "id");
			   
			serializer.startTag(null, "name");
			//set an attribute called "attribute" with a "value" for <child2>
			serializer.text("RebelAida");
			serializer.endTag(null, "name");
			   
			serializer.startTag(null, "score");
			//write some text inside <child3>
			serializer.text("30000");
			serializer.endTag(null, "score");
			serializer.endTag(null, "result");    
			serializer.startTag(null, "result");
			//i indent code just to have a view similar to xml-tree
			serializer.startTag(null, "id");
			serializer.text("2");
			serializer.endTag(null, "id");
			
			serializer.startTag(null, "name");
			//set an attribute called "attribute" with a "value" for <child2>
			serializer.text("Zunder3D");
			serializer.endTag(null, "name");
			
			serializer.startTag(null, "score");
			//write some text inside <child3>
			serializer.text("20000");
			serializer.endTag(null, "score");
			serializer.endTag(null, "result");
			serializer.startTag(null, "result");
			//i indent code just to have a view similar to xml-tree
			serializer.startTag(null, "id");
			serializer.text("3");
			serializer.endTag(null, "id");
			 
			serializer.startTag(null, "name");
			//set an attribute called "attribute" with a "value" for <child2>
			serializer.text("Lukethor");
			serializer.endTag(null, "name");			 
			serializer.startTag(null, "score");
			
			//write some text inside <child3>
			serializer.text("100");
			serializer.endTag(null, "score");
			serializer.endTag(null, "result");
			serializer.endTag(null, "results");
			serializer.endDocument();
			
			//write xml data into the FileOutputStream
			serializer.flush();
			
			//finally we close the file stream
			fileos.close();
		                       
		} catch (Exception e) {
			Toaster("Error occurred while creating xml file");
			return "error";
		}
		
		return read();
	}
	
	public static String read() throws IOException{
		FileInputStream fIn = new FileInputStream(FrogListScore.xmlPath + "/" + FrogListScore.xmlFile);
		
		int ch;
		StringBuffer strContent = new StringBuffer("");
		
		while( (ch = fIn.read()) != -1)
	        strContent.append((char)ch);
		
		return strContent.toString();
	}
	
	public static boolean save(Document doc, String name, int score, int index){
	
		// create a File object for the output file		
		File outputFile = new File(FrogListScore.xmlPath, FrogListScore.xmlFile);
		
		FileOutputStream fileos = null;
		 
		try{
			fileos = new FileOutputStream(outputFile,false);		
		}catch(IOException e){
			Toaster("Cannot open file '"+ outputFile.getAbsolutePath().toString() +"'");
			return false;
		}
		
		//we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		NodeList nodes = doc.getElementsByTagName("result");
		try {
			//we set the FileOutputStream as output for the serializer, using UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			//Write <?xml declaration with encoding (if encoding not null) and standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			//set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			//start a tag called "root"
			serializer.startTag(null, "results");			
			serializer.attribute("", "count", Integer.toString(XMLfunctions.numResults(doc)));
			
			for (int i = 0; i < nodes.getLength(); i++) {
				
				Element e = (Element)nodes.item(i);	
				
				String id = "";
				String newname = "";
				String newscore = "";
				
				if(index == i){
					id = Integer.toString(i+1);
					newname = name;
					newscore = Integer.toString(score);
				}else{
					id = Integer.toString(i+1);
					newname = XMLfunctions.getValue(e, "name");
					newscore = XMLfunctions.getValue(e, "score");
				}
								
				serializer.startTag(null, "result");
				
					serializer.startTag(null, "id");
						serializer.text(id);
					serializer.endTag(null, "id");
					   
					serializer.startTag(null, "name");
						serializer.text(newname);
					serializer.endTag(null, "name");
					   
					serializer.startTag(null, "score");
						serializer.text(newscore);
					serializer.endTag(null, "score");
				
				serializer.endTag(null, "result");   
			
			}
			
			serializer.endTag(null, "results");
			serializer.endDocument();
			
			//write xml data into the FileOutputStream
			serializer.flush();
			
			//finally we close the file stream
			fileos.close();
		                       
		} catch (Exception e) {
			Toaster("Error occurred while creating xml file");
			return false;
		}
		
		return true;
	}
	
	private static void Toaster(String msg){
		Log.i(FrogListScore.LOGTAG, msg);
		Toast.makeText(FrogListScore.CONTEXT, msg, Toast.LENGTH_SHORT);	
	}

	public static boolean delete() {
		File outputFile = new File(FrogListScore.xmlPath, FrogListScore.xmlFile);		
		return outputFile.delete();
	}
}
