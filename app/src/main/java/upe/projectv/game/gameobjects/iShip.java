/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package upe.projectv.game.gameobjects;

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;

/**
 *
 * @author jbomb
 */
public interface iShip {
    
    void saveStatus();
    void setObject3dContainer(Object3dContainer o);
    Object3dContainer getObject3dContainer();
    void setShipTranslation(Number3d n);
    void setObjTranslation(Number3d n);
    float[] getBoundingBox();
    Number3d getLocation();
    void setStatus(int status);
    int getStatus();
    void setGroupNumber(int group);
    int getGroupNumber();
    
    int getType();
    
    iExplosion getExplosion();
    void setExplosion(iExplosion explosion);
    
    // for shot counting
    int getShotDelayCount();
    void setShotDelayCount(int shotDelay);
}
