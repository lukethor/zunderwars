package upe.projectv.game.gameobjects;

/**
 *
 * @author jbomb
 */

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;

public interface iExplosion {
 
    void setObject3dContainer(Object3dContainer o);
    //void setObject3dContainer();
    void saveStatus();
    Object3dContainer getObject3dContainer();
    //void cloneObject3dContainer(Object3dContainer o);
    //void setObjectPosition(float x,float y,float z);
    void setTranslation(float x,float y,float z);
    //void setShip(iShip ship);
    //iShip getShip();
    int cycle();
}
