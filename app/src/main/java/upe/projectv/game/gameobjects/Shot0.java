package upe.projectv.game.gameobjects;

import min3d.core.Object3dContainer;
import min3d.vos.Number3d;
import min3d.objectPrimitives.Rectangle;
import min3d.vos.Color4;
import min3d.core.Object3d;

public class Shot0 implements iShot {
    
    private Object3dContainer obj;
    private Number3d translate;
    private Number3d rotate;
    
    private float[] bounds;
    
    public Shot0() {
        
        // set the bounding box (for collision detection)
        bounds = new float[] {-0.175f,0.175f,-1.0f,1.0f};
        
        // set default position
        translate = new Number3d(0,-10,-12f);
        rotate = new Number3d(90,0,0);
    }
    
    // this loads the 3D object. each time we return we'll need to do this
    @Override
    public void setObject3dContainer(Object3dContainer o) {
        obj = o.clone();
        
        obj.position().setAllFrom(translate);
        obj.rotation().setAllFrom(rotate);
    }
    
    // before going into pause, we need to save the current status of the
    // 3D object (position, rotation, etc.)
    @Override
    public void saveStatus() {
        translate.setAllFrom(obj.position());
        rotate.setAllFrom(obj.rotation());
    }
    
    @Override
    public Object3dContainer getObject3dContainer() {
        return obj;
    }
    
    @Override
    public void setTranslation(Number3d n) {
        obj.position().setAllFrom(n);
    }
    
    @Override
    public void setTranslation(float x,float y,float z) {
        obj.position().x = x;
        obj.position().y = y;
        obj.position().z = z;
        
        /*
        obj.position().x = new Float(x);
        obj.position().y = new Float(y);
        obj.position().z = new Float(z);
        */
    }
    
    @Override
    public float[] getBoundingBox() {
        return bounds;
    }
    
    @Override
    public Number3d getLocation() {
        //return translate;
        return obj.position();
    }
    
    @Override
    public void gotHit() {
        //lifeStatus = 1;
    }
    
    @Override
    public int getLifeStatus() {
        //return lifeStatus;
        return 0;
    }
}
