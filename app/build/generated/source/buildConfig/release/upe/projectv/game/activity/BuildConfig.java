/**
 * Automatically generated file. DO NOT MODIFY
 */
package upe.projectv.game.activity;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "upe.projectv.game.activity";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "2.0";
}
